.text
.globl main

main:
    # Load immediate values into registers
    li $t0, 5
    li $t1, 10

    # Branch if equal (beq)
    beq $t0, $t1, label1 # If $t0 is equal to $t1, jump to label1

    # Jump (j)
    j label2 # Unconditional jump to label2

label1:
    # Load immediate value into register
    li $v0, 1

    # Jump and link (jal)
    jal func1 # Jump to func1 and link return address to $ra

    # Jump register (jr)
    jr $ra # Jump to address contained in register $ra

label2:
    # Branch if not equal (bne)
    bne $t0, $t1, end # If $t0 is not equal to $t1, jump to end

end:
    li $v0, 10 # Exit program
    syscall

func1:
    jal func2 # Jump to func2 and link return address to $ra
    jr $ra # Jump to address contained in register $ra

func2:
    jal func3 # Jump to func3 and link return address to $ra
    jr $ra # Jump to address contained in register $ra

func3:
    jal func4 # Jump to func4 and link return address to $ra
    jr $ra # Jump to address contained in register $ra

func4:
    jr $ra # Jump to address contained in register $ra

