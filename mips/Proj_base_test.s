.data
var1: .word 5
var2: .word 10

.text
main:
    # Load upper immediate (lui)
    lui $t0, 0x1001

    # Load word (lw)
    lw $t1, var1
    lw $t2, var2

    # Arithmetic instructions (add, addi, addiu, addu, sub, subu)
    add $t3, $t1, $t2
    addi $t4, $t3, 10
    addiu $t5, $t4, 10
    addu $t6, $t5, $t0
    sub $t7, $t6, $t0
    subu $t0, $t7, $t0

    # Logical instructions (and, andi, nor, xor, xori, or, ori)
    and $t1, $t0, $t0
    andi $t2, $t1, 15
    nor $t3, $t2, $t1
    xor $t4, $t3, $t2
    xori $t5, $t4, 10#This isn't coherant but should demonstrate  they work
    or 	$t6,$t5,$t4   
	ori	$t7,$t6,16   

	# Shift instructions (slt,sll,srl,sra)
	#slt	$t8,$t7,$t6   
	sll	$t9,$t2,10   
	srl	$t5,$t3,3   
	#sra	$t9,$t7,3   

	# Branch instructions (beq,bne,j,jal,jr)
	beq	$t9,$t9,label4  
	bne	$t9,$t2,label2 
	j	label3          
	jal	label2         
	jr	$ra            
	j	label
label:
	j	end             

label2:
	sw	$t5,4($t5)       
	j	end             

label3:
	sw	$t7,4($t5)       

label4:
	li	$t5,10
	la	$t6,var1
	sw	$t5,0($t6)       

end:
