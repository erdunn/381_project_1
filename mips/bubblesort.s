.data
.align 2
values: .word 5, 7, 15, 81, 38, 1, 327, 51, 40
values_length: .word 10
.text
.globl main

# NO-OP - nop

main:
    lui $s1, 0x1001
    lui $s0, 0x1001
    addi $s2, $zero, 0
    nop
    ori $s1, $s1, 0x0028  
    nop 
    nop
    nop
    lw $s1, 0($s1)   
    nop  
    nop   
    nop
outer_loop_cond:
    addi $t0, $s1, -1
    nop
    nop
    nop
    slt $t1, $s2, $t0
    nop
    nop
    nop
    bne $t1, $zero, outer_loop_body
    j exit_outer_loop
outer_loop_body:
    add $s3, $zero, $zero
    add $s4, $zero, $zero
inner_loop_cond:
    sub  $t0, $s1, $s2
    nop
    nop
    nop
    addi $t0, $t0, -1
    nop
    nop
    nop
    slt $t0, $s3, $t0
    nop
    nop
    nop
    bne $t0, $zero, inner_loop_body
    j exit_inner_loop
inner_loop_body:
    sll $t0, $s3, 2
    nop
    nop
    addi $s6, $s5, 4
    add $s5, $s0, $t0
    nop
    nop
    nop
    nop
    lw $t1, 0($s6)
    lw $t0, 0($s5)
    nop
    nop
    nop
    slt $t2, $t1, $t0
    nop
    nop
    nop
    beq $t2, $zero, inner_loop_footer
    sw $t1, 0($s5)
    addi $s4, $zero, 1
    sw $t0, 0($s6)
inner_loop_footer:
    addi $s3, $s3, 1
    j inner_loop_cond
exit_inner_loop:
    beq $s4, $zero, exit_outer_loop
outer_loop_footer:
    addi $s2, $s2, 1
    j outer_loop_cond
exit_outer_loop:
    j exit
exit:
    halt
