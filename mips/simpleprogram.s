.globl main
main:
      jal first
      la, $s0, 4
      addi $s0, $0, 3
      ori  $s1, $0, 8
      slti $s2, $0, 1
      andi $s3, $s0, 15
      nor $s4, $s0, $s1
      sub $s5, $s1, $s0
      beq $s5, $s5, then

then:
      lui $s0, 0xffff
      srl $s1, $s3, 2
      sll $s2, $s4, 4
      sra $s3, $s5, 2
      lw  $s4, 0($s0)
      bne $s4, $zero, loop

loop:
      sw $s0, 0($s2)
      j next
next:
      xori $s1, $s1, 127
      jal after
first:
      jr $ra
after:
      halt

