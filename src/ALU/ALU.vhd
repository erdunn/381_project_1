library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity ALU is
	generic(N : integer := 32);
	port(i_A	: in std_logic_vector(N-1 downto 0); 
	     i_B	: in std_logic_vector(N-1 downto 0);        --second input used from mux with sign extender
	     i_ALUOP	: in std_logic_vector(4 downto 0);          --4 bit aluop code to determine operation
	     i_shamt    : in std_logic_vector(4 downto 0);          --shift amount used
	     o_F	: out std_logic_vector(N-1 downto 0);
	     o_Overflow	: out std_logic;
	     o_Zero	: out std_logic);
end ALU;

architecture structure of ALU is

component bShifter
	port(i_shamt : in STD_LOGIC_VECTOR(4 downto 0);
        i_X   : in STD_LOGIC_VECTOR(N-1 downto 0); -- Input data
        i_S0  : in STD_LOGIC;      -- Control signal S0
        i_S1  : in STD_LOGIC;      -- Control signal S1
        o_Y   : out STD_LOGIC_VECTOR(N-1 downto 0)); -- Output data
end component;

component ALU_Control
	port(i_ALUOP	: in std_logic_vector(4 downto 0);
	     i_AND	: in std_logic_vector(31 downto 0);
	     i_OR	: in std_logic_vector(31 downto 0);
	     i_NOR	: in std_logic_vector(31 downto 0);
	     i_XOR	: in std_logic_vector(31 downto 0);
	     i_AddSub	: in std_logic_vector(31 downto 0);
	     i_sll	: in std_logic_vector(31 downto 0);
	     i_srl	: in std_logic_vector(31 downto 0);
	     i_sra 	: in std_logic_vector(31 downto 0);
	     i_slt	: in std_logic_vector(31 downto 0);
		 i_B	: in std_logic_vector(31 downto 0);
		 i_usignA: in std_logic_vector(N-1 downto 0);
		 i_ANDI : in std_logic_vector(N-1 downto 0);
		 i_ORI	:  in std_logic_vector(N-1 downto 0);
		 i_XORI	:  in std_logic_vector(N-1 downto 0);
	     o_F	: out std_logic_vector(N-1 downto 0));
end component;

component add_sub_N
	generic(N : integer := 32);
	port(i_X	: in std_logic_vector(N-1 downto 0);
	     i_Y 	: in std_logic_vector(N-1 downto 0);
	     i_N 	: in std_logic;
	     o_S 	: out std_logic_vector(N-1 downto 0);
	     o_C 	: out std_logic);
end component;

component org2_32Bit  --32 bit
	port(i_A    : in std_logic_vector(31 downto 0);
       i_B          : in std_logic_vector(31 downto 0);
       o_F          : out std_logic_vector(31 downto 0));
end component;

component andg2_32Bit --32 bit
	port(i_A    : in std_logic_vector(31 downto 0);
       i_B          : in std_logic_vector(31 downto 0);
       o_F          : out std_logic_vector(31 downto 0));
end component;

component xorg2_32Bit --32 bit
	port(i_A    : in std_logic_vector(31 downto 0);
       i_B          : in std_logic_vector(31 downto 0);
       o_F          : out std_logic_vector(31 downto 0));
end component;

component norg2_32Bit --32 bit
	port(i_A    : in std_logic_vector(31 downto 0);
       i_B          : in std_logic_vector(31 downto 0);
       o_F          : out std_logic_vector(31 downto 0));
end component;

component zero_extender
    generic (
        INPUT_WIDTH : natural := 16
    );
    port (
        inputs : in std_logic_vector(INPUT_WIDTH - 1 downto 0);
        outputs : out std_logic_vector(31 downto 0)
    );
end component;

component andg2
    port(i_A    : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
end component;

  signal s_AND : std_logic_vector(31 downto 0);
  signal s_OR  : std_logic_vector(31 downto 0);
  signal s_NOR : std_logic_vector(31 downto 0);
  signal s_XOR : std_logic_vector(31 downto 0);
  signal s_sll : std_logic_vector(31 downto 0);
  signal s_srl : std_logic_vector(31 downto 0);
  signal s_sra,s_ANDI,s_ORI,s_XORI, s_A, s_B : std_logic_vector(31 downto 0);
  signal s_slt,s_Bs,s_temp : std_logic_vector(31 downto 0);
  signal s_AddSub : std_logic_vector(31 downto 0);
  signal s_AddSubC : std_logic;
  signal s_AddOrSub : std_logic;
  signal s_unsign1,s_unsign2,s_usignS: std_logic_vector(31 downto 0);
  signal s_usignC,s_usignO : std_logic;
  signal s_zero0 : std_logic;
  signal s_overflowControl, s_overflow: std_logic;

begin
	
	s_Bs <= i_B(15 downto 0) & X"0000"; -- lui
	
	--s_usignC <= i_ALUOP(2); 
	s_A <= i_A;
	s_B <= i_B;


o_Overflow <= '0' WHEN i_ALUOP = "00011" OR i_ALUOP = "00100" OR i_ALUOP = "00001" OR i_ALUOP = "00010" OR i_ALUOP = "00000" OR i_ALUOP = "00101" OR i_ALUOP = "00111" OR i_ALUOP = "01111" OR i_ALUOP = "01001" OR i_ALUOP = "10100" OR i_ALUOP = "01101" OR i_ALUOP = "10000" OR i_ALUOP = "10001" OR i_ALUOP ="10010" OR i_ALUOP ="10011" OR i_ALUOP ="10100" OR i_ALUOP ="10101" OR i_ALUOP = "01011" OR i_ALUOP = "01100" OR i_ALUOP = "10111" OR i_ALUOP = "01010" ELSE
			'0' WHEN i_A = X"00000000" ELSE --Garbage hack job for specific edge case. Don't Move lower
			'1' WHEN i_A(31) = '0' AND i_B(31) = '0' AND o_F(31) = '1' ELSE
			'1' WHEN i_A(31) = '1' AND i_B(31) = '1' AND o_F(31) = '0' AND i_ALUOP /= "01110" ELSE
			'1' WHEN i_A(31) = '1' AND i_B(31) = '0' AND o_F(31) = '0' ELSE
			'1' WHEN i_A(31) = '0' AND i_B(31) = '1' AND o_F(31) = '1' AND ((i_ALUOP /= "00110" ) OR ( i_ALUOP /= "10110" ) OR ( i_ALUOP /= "10111" )) ELSE
		'0';


	-- with i_aluOp select s_overflowControl <=
	-- 	'1' when "00110",
	-- 	'1' when "01110",
	-- 	'0' when others;
	
	-- m_overflowAnd: andg2
	-- 	port Map(i_A    => s_overflowControl,
	-- 		 i_B    => s_AddSubC,
	-- 		 o_F    => s_overflow);
	
	

	--Don't convert to 2s complement
	--check for overflow
	--control
	

	--o_Overflow <= s_usignO;

-- m_zero_exA: zero_extender
-- 	port Map(
-- 		inputs => i_A(31 downto 16),
-- 		outputs => s_unsign1

-- 	);		
-- m_zero_exB: zero_extender
-- 	port Map(
-- 		inputs => i_B(31 downto 16),
-- 		outputs => s_unsign2

-- 	);
-- m_addsub_usign: add_sub_N
-- 	port MAP(i_X	=> i_A,
-- 		 i_Y	=> i_B,
-- 		 i_N	=> s_AddOrSub,             --0 or 1 depending on addition or subtraction
-- 		 o_S	=> s_AddSub,
-- 		 o_C	=> s_AddSubC);

m_and: andg2_32Bit
	port Map(i_A => i_A,
		 i_B => i_B,
		 o_F => s_AND);

m_or: org2_32Bit
	port Map(i_A => i_A,
		 i_B => i_B,
		 o_F => s_OR);

m_xor: xorg2_32Bit
	port Map(i_A => i_A,
		 i_B => i_B,
		 o_F => s_XOR);
m_xori: xorg2_32Bit
	port Map(i_A => i_A,
		 i_B => i_B,
		 o_F => s_XORI);
m_nor: norg2_32Bit
	port Map(i_A => i_A,
		 i_B => i_B,
		 o_F => s_NOR);
m_andi: andg2_32Bit
	port Map(i_A => i_A,
		i_B => i_B,
		o_F => s_ANDI);
m_ori: org2_32Bit
	port Map(i_A => i_A,
		i_B => i_B,
		o_F => s_ORI);
--01010 BEQ
	s_AddOrSub <= i_ALUOP(3); -- 0 means Add, 1 means Subtract, test this

m_addsub: add_sub_N
	port MAP(i_X	=> i_A,
		 i_Y	=> i_B,
		 i_N	=> s_AddOrSub,             --0 or 1 depending on addition or subtraction
		 o_S	=> s_AddSub,
		 o_C	=> s_AddSubC);
		 
	--	 o_Overflow <= s_overflow;
	--s_slt <= X"00000001" WHEN (s_overflow XOR s_AddSub(31)) ELSE
	--X"00000000";
	--s_slt <= X"00000001" WHEN (i_B > i_A) ELSE
	--X"00000000";
	s_slt <= X"00000001" WHEN (i_A(31) = '0' AND i_B(31) = '0' AND i_B > i_A) ELSE
			 X"00000001" WHEN (i_A(31) = '1' AND i_B(31) = '0' AND i_A > i_B) ELSE
			 X"00000000" WHEN (i_B(31) = '1' AND i_A(31) = '0' AND i_B > i_A) ELSE
			 X"00000000" WHEN (i_A(31) = '1' AND i_B(31) = '1' AND i_A > i_B) ELSE
			 X"00000001" WHEN (i_B > i_A) ELSE
			 X"00000000";

	o_Zero <= not(s_AddSub(0) or s_AddSub(1) or s_AddSub(2) or s_AddSub(3) or s_AddSub(4) or s_AddSub(5) or
		      s_AddSub(6) or s_AddSub(7) or s_AddSub(8) or s_AddSub(9) or s_AddSub(10) or s_AddSub(11) or
		      s_AddSub(12) or s_AddSub(13) or s_AddSub(14) or s_AddSub(15) or s_AddSub(16) or s_AddSub(17) or
		      s_AddSub(18) or s_AddSub(19) or s_AddSub(20) or s_AddSub(21) or s_AddSub(22) or s_AddSub(23) or
		      s_AddSub(24) or s_AddSub(25) or s_AddSub(26) or s_AddSub(27) or s_AddSub(28) or s_AddSub(29) or
		      s_AddSub(30) or s_AddSub(31));

	

	-- process (i_ALUOP, s_zero0)
	-- begin
	-- 	if i_ALUOP = "01001" and s_zero0 = '1' then--bne
	-- 		o_Zero <= '0';
	-- 	elsif i_ALUOP = "01010" and s_zero0 = '0' then --beq
	-- 		o_Zero <= '1';
	-- 	elsif i_ALUOP = "10000" then
	-- 		o_Zero <= '1';
	-- 	else
	-- 		o_Zero <= s_zero0;
	-- 	end if;
	-- end process;
			



	--o_Overflow <= s_AddSubC; TODO

m_shiftleft: bShifter                             --gives sll amount
	port Map(i_shamt => i_shamt,
        	 i_X => i_B,
        	 i_S0 => '1',
        	 i_S1 => '0',
        	 o_Y => s_sll);

m_shiftright: bShifter                             --gives srl amount
	port Map(i_shamt => i_shamt,
        	 i_X => i_B,
        	 i_S0 => '0',
        	 i_S1 => '0',
        	 o_Y => s_srl);

m_shiftrightlogical: bShifter                             --gives sra amount
	port Map(i_shamt => i_shamt,
        	 i_X => i_B,
        	 i_S0 => '0',
        	 i_S1 => '1',
        	 o_Y => s_sra);





m_ALU_Control: ALU_Control
	port MAP(i_ALUOP	=> i_ALUOP,
		 i_AND		=> s_AND,
	         i_OR		=> s_OR,
	         i_NOR		=> s_NOR,
	         i_XOR		=> s_XOR,
	         i_AddSub	=> s_AddSub,
	         i_sll  	=> s_sll,
		 i_srl  	=> s_srl,
		 i_sra  	=> s_sra,
		 i_slt		=> s_slt,
		 i_B		=> s_Bs,
		 i_usignA	=> s_usignS,
		 i_ANDI		=> s_ANDI,
		 i_ORI		=> s_ORI,
		 i_XORI		=> s_XORI,
	         o_F		=> o_F);


end structure;

