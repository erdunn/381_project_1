library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity ALU_Control is
	generic(N : integer := 32);
	port(i_ALUOP	: in std_logic_vector(4 downto 0);
	     i_AND	: in std_logic_vector(N-1 downto 0);
	     i_OR	: in std_logic_vector(N-1 downto 0);
	     i_NOR	: in std_logic_vector(N-1 downto 0);
	     i_XOR	: in std_logic_vector(N-1 downto 0);
	     i_AddSub	: in std_logic_vector(N-1 downto 0);
	     i_sll	: in std_logic_vector(N-1 downto 0);
	     i_srl	: in std_logic_vector(N-1 downto 0);
	     i_sra 	: in std_logic_vector(N-1 downto 0);
	     i_slt	: in std_logic_vector(N-1 downto 0);
		 i_B	: in std_logic_vector(N-1 downto 0);
		 i_usignA: in std_logic_vector(N-1 downto 0);
		 i_ANDI : in std_logic_vector(N-1 downto 0);
		 i_ORI	:  in std_logic_vector(N-1 downto 0);
		 i_XORI	:  in std_logic_vector(N-1 downto 0);
	     o_F	: out std_logic_vector(N-1 downto 0));
end ALU_Control;

architecture structure of ALU_Control is

--00000 and
--00001 or
--00010 xor
--00011 nor
--00100 sll
--00101 srl
--00110 add
--00111 sra
--01000 
--01001 bne
--01010 beq 
--01100 subu
--01011 addu 
--01101 lui
--01110 sub
--01111 slt
--01101 lui
--10000 j probably not needed
--10001 jal
--10010 jr
--10011 andi // TODO ANDI
--10100 ori
--10101 xori
--10110 addi
--10111 addiu


--Unified the ALUOP codes for addu/addi/addui etc. Write value being incorrectly read from the testbench resulting in a fail where there shouldn't be 
--Remove process statement.
begin
o_F <= i_AND when i_ALUOP = "00000" else
		i_OR when i_ALUOP = "00001" else
		i_XOR when i_ALUOP = "00010" else
		i_NOR when i_ALUOP = "00011" else
		i_AddSub when i_ALUOP = "00110" else
		i_AddSub when i_ALUOP = "01110" else
		i_sll when i_ALUOP = "00100" else
		i_srl when i_ALUOP = "00101" else
		i_sra when i_ALUOP = "00111" else
		i_slt when i_ALUOP = "01111" else
		i_B when i_ALUOP = "01101"else
		i_ANDI when i_ALUOP = "10011"else
		i_ORI when i_ALUOP = "10100"else
		i_XORI when i_ALUOP = "10101"else
		i_AddSub when i_ALUOP = "01100" else
		i_AddSub when i_ALUOP = "01011" OR i_ALUOP = "10110" OR i_ALUOP = "10111" else
		x"00000000";




 
-- process(i_ALUOP, i_AND, i_OR, i_NOR, i_XOR, i_AddSub, i_sll, i_srl, i_sra, i_slt, i_B,i_usignA)
-- begin
-- if i_ALUOP = "00000" then --AND
--    for i in 0 to 31 loop
-- 	o_F(i) <= i_AND(i);
--    end loop;
-- elsif i_ALUOP = "00001" then --OR
--    for i in 0 to 31 loop
-- 	o_F(i) <= i_OR(i);
--    end loop;
-- elsif i_ALUOP = "00010" then --XOR
--    for i in 0 to 31 loop
-- 	o_F(i) <= i_XOR(i);
--    end loop;
-- elsif i_ALUOP = "00011" then --NOR
--    for i in 0 to 31 loop
-- 	o_F(i) <= i_NOR(i);
--    end loop;
-- elsif i_ALUOP = "00110" then --ADD
--    for i in 0 to 31 loop
-- 	o_F(i) <= i_AddSub(i);
--    end loop;
-- elsif i_ALUOP = "01110" then --SUB
--    for i in 0 to 31 loop
-- 	o_F(i) <= i_AddSub(i);
--    end loop;
-- elsif i_ALUOP = "00100" then --SLL
--    for i in 0 to 31 loop
-- 	o_F(i) <= i_sll(i);
--    end loop;
-- elsif i_ALUOP = "00101" then --SRL
--    for i in 0 to 31 loop
-- 	o_F(i) <= i_srl(i);
--    end loop;
-- elsif i_ALUOP = "00111" then --SRA
--    for i in 0 to 31 loop
-- 	o_F(i) <= i_sra(i);
--    end loop;
-- elsif i_ALUOP = "01111" then --SLT
--    for i in 0 to 31 loop
-- 	o_F(i) <= i_slt(i);
--    end loop;
-- elsif i_ALUOP = "01101" then --lui
--    for i in 0 to 31 loop
-- 	o_F(i) <= i_B(i);
--    end loop;
-- else 
-- 	o_F <= x"00000000";
-- end if;
-- end process;
end structure;
