library IEEE;
use IEEE.std_logic_1164.all;

entity add_sub_N is
	generic(N : integer := 32);
	port(i_X	: in std_logic_vector(N-1 downto 0);
	     i_Y 	: in std_logic_vector(N-1 downto 0);
	     i_N	: in std_logic;
	     o_S 	: out std_logic_vector(N-1 downto 0);
	     o_C 	: out std_logic);
end add_sub_N;

architecture structure of add_sub_N is

component full_adder_N is
	port(i_Xv	: in std_logic_vector(N-1 downto 0);
	     i_Yv 	: in std_logic_vector(N-1 downto 0);
	     i_Cv 	: in std_logic;
	     o_Sv 	: out std_logic_vector(N-1 downto 0);
	     o_Cv 	: out std_logic);
end component;

component mux2t1_N
	port(i_S          : in std_logic;
	     i_D0         : in std_logic_vector(31 downto 0);
	     i_D1         : in std_logic_vector(31 downto 0);
	     o_O          : out std_logic_vector(31 downto 0));
end component;

component ones_comp
	port(i_D0	: in std_logic_vector(N-1 downto 0);
             o_O	: out std_logic_vector(N-1 downto 0));
end component;

signal s_T0	: std_logic_vector(N-1 downto 0);
signal s_T1	: std_logic_vector(N-1 downto 0);
signal s_temp	: std_logic;
signal s_temp2	: std_logic_vector(N-1 downto 0);
signal s_Out	: std_logic_vector(N-1 downto 0);

begin

m_Inverter: ones_comp
	port MAP(i_D0	=> i_Y,
		 o_O	=> s_T0);

m_multiplexer: mux2t1_N
	port MAP(i_S	=> i_N,
		 i_D0	=> i_Y,
		 i_D1	=> s_T0,
		 o_O	=> s_T1);

m_adder1: full_adder_N
	port MAP(i_Xv	=> i_X,
	         i_Yv	=> s_T1,
	         i_Cv	=> i_N,
	         o_Sv	=> o_S,
	         o_Cv	=> o_C);

--s_temp2 <= ("0000000000000000000000000000000" & s_temp);
--m_adder2: full_adder_N
--	port MAP(i_Xv	=> s_temp2,
--	         i_Yv	=> s_Out,
--	         i_Cv	=> '0',
--	         o_Sv	=> o_S,
--	         o_Cv	=> o_C);

end structure;