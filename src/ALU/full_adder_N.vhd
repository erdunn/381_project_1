library IEEE;
use IEEE.std_logic_1164.all;

entity full_adder_N is
	generic(N : integer := 32);
	port(i_Xv	: in std_logic_vector(N-1 downto 0);
	     i_Yv 	: in std_logic_vector(N-1 downto 0);
	     i_Cv 	: in std_logic;
	     o_Sv 	: out std_logic_vector(N-1 downto 0);
	     o_Cv 	: out std_logic);
end full_adder_N;

architecture structure of full_adder_N is

component full_adder is
port(i_X 	: in std_logic;
     i_Y 	: in std_logic;
     i_C 	: in std_logic;
     o_S 	: out std_logic;
     o_C 	: out std_logic);
end component;

signal s_T : std_logic_vector(N downto 0);


begin 

	s_T(0) <= i_Cv;
	o_Cv   <= s_T(N);

	G_NBit_ADDER: for i in 0 to N-1 generate
	ADDER: full_adder port map(
		i_X	=> i_Xv(i),
		i_Y	=> i_Yv(i),
		i_C	=> s_T(i),
		o_S	=> o_Sv(i),
		o_C	=> s_T(i+1));

	end generate G_NBit_ADDER;

end structure;
