library IEEE;
use IEEE.std_logic_1164.all;

entity mux_2t1 is
	port(
	     i_S  : in std_logic;
	     i_D1 : in std_logic;
             i_D0 : in std_logic;
             o_O  : out std_logic);
end mux_2t1;


architecture structure of mux_2t1 is

component invg
	port(i_A		: in std_logic;
	     o_F		: out std_logic);
end component;

component andg2
	port(i_A          : in std_logic;
             i_B          : in std_logic;
             o_F          : out std_logic);
end component;

component org2
	port(i_A          : in std_logic;
             i_B          : in std_logic;
             o_F          : out std_logic);
end component;

signal s_n	: std_logic;
signal s_x	: std_logic;
signal s_y	: std_logic;

begin

m_notGate: invg
	port MAP(i_A	=> i_S,
		 o_F	=> s_n);

m_andGate1: andg2
	port MAP(i_A	=> i_D0,
		 i_B	=> s_n,
		 o_F	=> s_x);

m_andGate2: andg2
	port MAP(i_A	=> i_D1,
		 i_B	=> i_S,
		 o_F	=> s_y);

m_orGate: org2
	port MAP(i_A	=> s_x,
	         i_B	=> s_y,
		 o_F	=> o_O);

end structure;