library IEEE;
use IEEE.std_logic_1164.all;

entity norg2_32Bit is

  port(i_A          : in std_logic_vector(31 downto 0);
       i_B          : in std_logic_vector(31 downto 0);
       o_F          : out std_logic_vector(31 downto 0));

end norg2_32Bit;

architecture structural of norg2_32Bit is

component org2
	port(i_A          : in std_logic;
      	     i_B          : in std_logic;
     	     o_F          : out std_logic);
end component;

signal s_out : std_logic_vector(31 downto 0);

begin
G_32_BIT_NOR: for i in 0 to 31 generate
NOR32: org2 port map(
	i_A => i_A(i),
	i_B => i_B(i),
	o_F => s_out(i));
  	o_F(i) <= NOT s_out(i);

end generate G_32_BIT_NOR;
end structural;