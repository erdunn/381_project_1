library IEEE;
use IEEE.std_logic_1164.all;

entity xorg2_32Bit is

  port(i_A          : in std_logic_vector(31 downto 0);
       i_B          : in std_logic_vector(31 downto 0);
       o_F          : out std_logic_vector(31 downto 0));

end xorg2_32Bit;

architecture structural of xorg2_32Bit is

component xorg2
port(i_A          : in std_logic;
       i_B          : in std_logic;
       o_F          : out std_logic);
end component;

begin
G_32_BIT_XOR: for i in 0 to 31 generate
XOR32: xorg2 port map(
  	i_A => i_A(i),
	i_B => i_B(i),
	o_F => o_F(i));
end generate G_32_BIT_XOR;
end structural;