library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL; -- REWRITE WITHOUT PROCESS STATEMENTS, FIX DELTA CYCLE
entity bShifter is
    generic (
        N : integer := 32  -- Set N to the desired bit width
    );
    Port (
        i_X     : in STD_LOGIC_VECTOR(N-1 downto 0); -- Input data
        i_S0    : in STD_LOGIC;      -- Control signal S0 (0 for left, 1 for right)
        i_S1    : in STD_LOGIC;      -- Control signal S1 (0 for shift, 1 for rotate)
        i_shamt : in STD_LOGIC_VECTOR(4 downto 0); -- Shift amount (determines the number of bits to shift/rotate)
        o_Y     : out STD_LOGIC_VECTOR(N-1 downto 0) -- Output data
    );
end bShifter;

architecture Behavioral of bShifter is
    signal temp_Y : STD_LOGIC_VECTOR(N-1 downto 0);

begin
    process (i_X, i_S0, i_S1, i_shamt)
    begin
        temp_Y <= (others => '0'); -- Initialize the output to all zeros
        
        if i_S1 = '0' then
            -- Shift operation
            for i in 0 to N-1 loop
                if i_S0 = '0' then
                    -- Logical left shift by i_shamt positions
                    if i + to_integer(unsigned(i_shamt)) < N then
                        temp_Y(i) <= i_X(i + to_integer(unsigned(i_shamt)));
                    end if;
                else
                    -- Logical right shift by i_shamt positions
                    if i - to_integer(unsigned(i_shamt)) >= 0 then
                        temp_Y(i) <= i_X(i - to_integer(unsigned(i_shamt)));
                    end if;
                end if;
            end loop;
        else
            for i in 0 to N-1 loop
               -- Rotate left by i_shamt positions
               if i + to_integer(unsigned(i_shamt)) < N then
                   temp_Y(i) <= i_X(i + to_integer(unsigned(i_shamt)));
               end if;
               --temp_Y(i) <= i_X((i - to_integer(unsigned(i_shamt))) mod N);
           end loop;
           for i in 0 to N-1 loop
               if (31 + to_integer(unsigned(i_shamt))) >= (31 + i) then
                   temp_Y(31 - i) <= i_X(31);
               end if;
            end loop;
        end if;
    end process;

    o_Y <= temp_Y;
end Behavioral;
