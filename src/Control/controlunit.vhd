-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- controlunit.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains the implementation for a MIPS Control Unit.
--              The control unit takes in the opcode and funct fields of an Instruction
--              and outputs the control signals for the datapath.
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity controlunit is
    Port(
        opcode              : in  std_logic_vector(5 downto 0); -- Takes in the opcode field of an instruction
        funct               : in  std_logic_vector(5 downto 0); -- Takes in the funct field of an instruction
        regDst              : out std_logic; -- Determines whether the destination register is rt or rd
        branch              : out std_logic; -- Determines whether the instruction is a branch
        jump                : out std_logic; -- Determines whether the instruction is a jump
        jr                : out std_logic; -- Determines whether the instruction is a jump
        jal                : out std_logic; -- Determines whether the instruction is a jump
        memRead             : out std_logic; -- Determines whether the instruction reads memory
        memToReg            : out std_logic; -- Determines whether the instruction writes memory to register
        ALUOp               : out std_logic_vector(4 downto 0); -- Determines the ALU operation
        memWrite            : out std_logic; -- Sets Memory write
        ALUSrc              : out std_logic; -- Determines the second ALU operand
        regWrite            : out std_logic; -- Determines whether the instruction writes to a register
        extender            : out std_logic; -- determines whether a value is zero extended or sign extended
        halt                : out std_logic); -- Determines whether the instruction is a halt
end controlunit;

-- ALU Op Key
--00000 and
--00001 or
--00010 xor
--00011 nor
--00100 sll
--00101 srl
--00110 add
--00111 sra
--01000 
--01001 bne
--01010 beq 
--01100 subu
--01011 addu 
--01101 lui
--01110 sub
--01111 slt
--01101 lui
--10000 j probably not needed
--10001 jal
--10010 jr
--10011 andi // TODO ANDI
--10100 ori
--10101 xori
--10110 addi
--10111 addiu
architecture Behavioral of controlunit is
    signal temp : std_logic;
begin
    process (opcode, funct)
    begin
    case opcode is
        when "000000" => -- R-Type Instruction
        case funct is
            when "100000" => -- ADD
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "00110";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
    
            when "100001" => -- ADDU
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "10111";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
            when "100100" => -- AND
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "00000";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
            when "100111" => -- NOR
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "00011";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
            when "100101" => -- OR
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "00001";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
            when "101010" => -- SLT
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "01111";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
            when "000000" => -- SLL
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "00100";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
            when "000010" => -- SRL
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "00101";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
            when "000011" => -- SRA
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "00111";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
            when "100010" => -- SUB
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "01110";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
            when "100011" => -- SUBU
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "01100";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
            when "001000" => -- JR
                regDst       <= '1';
                branch       <= '0';
                jump         <= '1';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "10011";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '0';
                halt         <= '0';
                jal          <= '0';
                jr           <= '1';
                extender     <= '0';
            when "100110" => -- XOR
                regDst       <= '1';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "00010";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '1';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';


            when others => -- Error
                regDst       <= '0';
                branch       <= '0';
                jump         <= '0';
                memRead      <= '0';
                memToReg     <= '0';
                ALUOp        <= "01000";
                memWrite     <= '0';
                ALUSrc       <= '0';
                regWrite     <= '0';
                halt         <= '0';
                jal          <= '0';
                jr           <= '0';
                extender     <= '0';
        end case;
        when "000010" => -- J
            regDst       <= '1';
            branch       <= '0';
            jump         <= '1';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "10000";
            memWrite     <= '0';
            ALUSrc       <= '0';
            regWrite     <= '0';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '0';
        when "001000" => -- ADDI
            regDst       <= '0';
            branch       <= '0';
            jump         <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "10110";
            memWrite     <= '0';
            ALUSrc       <= '1';
            regWrite     <= '1';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '0';
        when "001001" => -- ADDIU -- FIXME
            regDst       <= '0';
            branch       <= '0';
            jump         <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "10111";
            memWrite     <= '0';
            ALUSrc       <= '1';
            regWrite     <= '1';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '0';
        when "001100" => -- ANDI
            regDst       <= '0';
            branch       <= '0';
            jump         <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "10011";
            memWrite     <= '0';
            ALUSrc       <= '1';
            regWrite     <= '1';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '1';
        when "001111" => -- LUI
            regDst       <= '0';
            branch       <= '0';
            jump         <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "01101";
            memWrite     <= '0';
            ALUSrc       <= '1';
            regWrite     <= '1';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '0';
        when "100011" => -- LW
            regDst       <= '0';
            branch       <= '0';
            jump         <= '0';
            memRead      <= '1';
            memToReg     <= '1';
            ALUOp        <= "00110";
            memWrite     <= '0';
            ALUSrc       <= '1';
            regWrite     <= '1';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '0';
        when "101011" => -- SW
            regDst       <= '0';
            branch       <= '0';
            jump         <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "00110";
            memWrite     <= '1';
            ALUSrc       <= '1';
            regWrite     <= '0';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '0';
        when "001101" => -- ORI
            regDst       <= '0';
            branch       <= '0';
            jump         <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "10100";
            memWrite     <= '0';
            ALUSrc       <= '1';
            regWrite     <= '1';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '1';
        when "001010" => -- SLTI
            regDst       <= '0';
            branch       <= '0';
            jump         <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "01111";
            memWrite     <= '0';
            ALUSrc       <= '1';
            regWrite     <= '1';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '0';
        when "000100" => -- BEQ
            regDst       <= '0';
            branch       <= '1';
            jump         <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "01010";
            memWrite     <= '0';
            ALUSrc       <= '0';
            regWrite     <= '0';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '0';
        when "000101" => -- BNE
            regDst       <= '0';
            branch       <= '1';
            jump         <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "01001";
            memWrite     <= '0';
            ALUSrc       <= '0';
            regWrite     <= '0';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '0';
        when "001110" => -- XORI
            regDst       <= '0';
            branch       <= '0';
            jump         <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "10101";
            memWrite     <= '0';
            ALUSrc       <= '1';
            regWrite     <= '1';
            halt         <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '1';
        
        when "000011" => -- JAL
            regDst       <= '0';
            branch       <= '0';
            jump         <= '1';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "10001";
            memWrite     <= '0';
            ALUSrc       <= '0';
            regWrite     <= '1';
            halt         <= '0';
            jal          <= '1';
            jr           <= '0';
            extender     <= '0';
        when "010100" => -- Halt
            regDst       <= '0';
            branch       <= '0';
            jump         <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "10000";
            memWrite     <= '0';
            ALUSrc       <= '0';
            regWrite     <= '0';
            halt         <= '1';
            jal          <= '0';
            jr           <= '0';
            extender     <= '0';
        when others => -- Other
            regDst       <= '0';
            branch       <= '0';
            memRead      <= '0';
            memToReg     <= '0';
            ALUOp        <= "10000";
            memWrite     <= '0';
            ALUSrc       <= '0';
            regWrite     <= '0';
            jal          <= '0';
            jr           <= '0';
            extender     <= '0';

    end case;
    end process;
end Behavioral;