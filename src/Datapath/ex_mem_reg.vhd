-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- ex_mem_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the EX/MEM Register.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity ex_mem_reg is
    generic(N : integer := 110);
    port(
        iCLK        : in std_logic;
        iRST        : in std_logic;
        iWE         : in std_logic;
        control     : in std_logic_vector(8 downto 0);
        pcPlus4     : in std_logic_vector(31 downto 0);
        regDst      : in std_logic_vector(4 downto 0); -- Stores Destination Register Address
        ALURes      : in std_logic_vector(31 downto 0);
        readData2   : in std_logic_vector(31 downto 0);
        oControl    : out std_logic_vector(8 downto 0);
        oPCPlus4    : out std_logic_vector(31 downto 0);
        oRegDst     : out std_logic_vector(4 downto 0);
        oALURes     : out std_logic_vector(31 downto 0);
        oReadData2  : out std_logic_vector(31 downto 0));
end ex_mem_reg;

architecture structual of ex_mem_reg is

    component register_N is
        generic(N : integer := 110);
        port(iCLK       : in std_logic;
             iRST       : in std_logic;
             iWE        : in std_logic;
             iD         : in std_logic_vector(N-1 downto 0);
             oQ         : out std_logic_vector(N-1 downto 0));
    end component;
    signal s_stall_31b_1,s_stall_31b_2,s_stall_31b_3 : std_logic_vector(31 downto 0);
    signal s_stall_5b : std_logic_vector(4 downto 0);
    signal s_stall_9b : std_logic_vector(8 downto 0);


begin
    --s_stall_9b <= (others => '0') when flush = '1' else control;
    --s_stall_31b_1 <=(others => '0') when flush = '1' else pcPlus4;
    --s_stall_5b <=(others => '0') when flush = '1' else regDst;
    --s_stall_31b_2 <=(others => '0') when flush = '1' else ALURes;
    --s_stall_31b_3 <=(others => '0') when flush = '1' else readData2;

    EX_MEM_Register: register_N 
        generic map(N => N)
        port map(
            iCLK    => iCLK,
            iRST    => iRST,
            iWE     => iWE,
            iD(109 downto 101)      => control,
            iD(100 downto 69)       => pcPlus4,
            iD(68 downto 64)        => regDst,
            iD(63 downto 32)        => ALURes,
            iD(31 downto 0)         => readData2,
            oQ(109 downto 101)      => oControl,
            oQ(100 downto 69)       => oPCPlus4,
            oQ(68 downto 64)        => oRegDst,
            oQ(63 downto 32)        => oALURes,
            oQ(31 downto 0)         => oReadData2
        );
end structual;
