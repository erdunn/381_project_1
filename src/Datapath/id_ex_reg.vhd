-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- id_ex_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the ID/EX Register.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

-- Needs Register Reads, Register Addresses, Sign Extend, Control Signals, and PC+4
entity id_ex_reg is 
    generic(N : integer := 164);
    port(
        iCLK        : in std_logic; -- Clock
        iRST        : in std_logic; -- Reset
        iWE         : in std_logic; -- Write Enable
        regdst      : in std_logic;
        iFlush      : in std_logic;
        shamt       : in std_logic_vector(4 downto 0); -- Shamt value for the ALU. 
        control     : in std_logic_vector(14 downto 0); -- Control Signals
        pcPlus4     : in std_logic_vector(31 downto 0); -- PC+4
        readData1   : in std_logic_vector(31 downto 0); -- Read Data 1 from Register File
        readData2   : in std_logic_vector(31 downto 0); -- Read Data 2 from Register File
        signExtend  : in std_logic_vector(31 downto 0); -- Sign Extended Immediate
        rsAddress   : in std_logic_vector(4 downto 0); -- Register Address 1
        rtAddress   : in std_logic_vector(4 downto 0); -- Register Address 2
        rdAddress   : in std_logic_vector(4 downto 0); -- Register Address 3
        oregdst     : out std_logic;
        oControl    : out std_logic_vector(14 downto 0); 
        oShamt       : out std_logic_vector(4 downto 0); -- Shamt value for the ALU. 
        oPcPlus4    : out std_logic_vector(31 downto 0);
        oReadData1  : out std_logic_vector(31 downto 0);
        oReadData2  : out std_logic_vector(31 downto 0);
        oSignExtend : out std_logic_vector(31 downto 0);
        oRsAddress  : out std_logic_vector(4 downto 0);
        oRtAddress  : out std_logic_vector(4 downto 0);
        oRdAddress  : out std_logic_vector(4 downto 0));
end id_ex_reg;

architecture structual of id_ex_reg is
    
    component register_N is
        generic(N : integer := 164);
        port(iCLK       : in std_logic;
             iRST       : in std_logic;
             iWE        : in std_logic;
             iD         : in std_logic_vector(N-1 downto 0);
             oQ         : out std_logic_vector(N-1 downto 0));
    end component;

        signal id_ex_stall_4 : std_logic;
        signal id_ex_stall_1, id_ex_stall_2, id_ex_stall_3, id_ex_stall_9 : std_logic_vector(31 downto 0);
        signal id_ex_stall_5, id_ex_stall_6,id_ex_stall_7,id_ex_stall_8 : std_logic_vector(4 downto 0);
        signal id_ex_stall_10 : std_logic_vector(14 downto 0);


begin
    id_ex_stall_4 <= '0' WHEN iFlush = '1' ELSE regdst;
    id_ex_stall_5 <= (others => '0') WHEN iFlush = '1' ELSE shamt;
    id_ex_stall_10 <= (others => '0') WHEN iFlush = '1' ELSE control;
    id_ex_stall_1 <= (others => '0') WHEN iFlush = '1' ELSE pcPlus4;
    id_ex_stall_2 <= (others => '0') WHEN iFlush = '1' ELSE readData1;
    id_ex_stall_3 <= (others => '0') WHEN iFlush = '1' ELSE readData2;
    id_ex_stall_9 <= (others => '0') WHEN iFlush = '1' ELSE signExtend;
    id_ex_stall_6 <= (others => '0') WHEN iFlush = '1' ELSE rsAddress;
    id_ex_stall_7 <= (others => '0') WHEN iFlush = '1' ELSE rtAddress;
    id_ex_stall_8 <= (others => '0') WHEN iFlush = '1' ELSE rdAddress;


    ID_EX_Register: register_N -- TODO, but I think I fixed it.
        generic map(N => N)
        port map(
            iCLK    => iCLK,
            iRST    => iRST,
            iWE     => iWE,
            iD(163)                 => id_ex_stall_4,
            iD(162 downto 158)      => id_ex_stall_5,
            iD(157 downto 143)      => id_ex_stall_10, -- bits 58,57,56,56,55 ALU OP
            iD(142 downto 111)      => id_ex_stall_1,
            iD(110 downto 79)       => id_ex_stall_2,
            iD(78 downto 47)        => id_ex_stall_3,
            iD(46 downto 15)        => id_ex_stall_9,
            iD(14 downto 10)        => id_ex_stall_6,
            iD(9 downto 5)         => id_ex_stall_7,
            iD(4 downto 0)          => id_ex_stall_8,
            oQ(163)                 => oregdst,
            oQ(162 downto 158)      => oShamt,
            oQ(157 downto 143)      => oControl,
            oQ(142 downto 111)      => oPcPlus4,
            oQ(110 downto 79)       => oReadData1,
            oQ(78 downto 47)        => oReadData2,
            oQ(46 downto 15)        => oSignExtend,
            oQ(14 downto 10)        => oRsAddress,
            oQ(9 downto 5)         => oRtAddress,
            oQ(4 downto 0)          => oRdAddress
        );
end structual;
