-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- if_id_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the IF/ID Register.
--              Page 304, 289, and 311 in the book
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

-- Needs Instruction, PC+4, Current PC, and Hazard Control Bits
entity if_id_reg is 
    generic(N : integer := 96);
    port(
        iCLK        : in std_logic; -- Clock
        iRST        : in std_logic; -- Reset
        iWE         : in std_logic; -- Write Enable
        iFlush      : in std_logic;
        currentPC   : in std_logic_vector(31 downto 0); -- Holds Current PC Address
        pcPlus4     : in std_logic_vector(31 downto 0); -- Holds PC+4 Address
        instruction : in std_logic_vector(31 downto 0); -- Holds Instruction
        oCurrentPC  : out std_logic_vector(31 downto 0); -- Outputs Current PC Address
        oPCPlus4    : out std_logic_vector(31 downto 0); -- Outputs PC+4 Address
        oInstruction: out std_logic_vector(31 downto 0)-- Outputs Instruction
        ); -- Outputs Flush
end if_id_reg;

architecture structual of if_id_reg is

    component register_N is
        generic(N : integer := 98);
        port(iCLK       : in std_logic;
             iRST       : in std_logic;
             iWE        : in std_logic;
             iD         : in std_logic_vector(N-1 downto 0);
             oQ         : out std_logic_vector(N-1 downto 0));
    end component;
        signal s_flush1,s_flush2, s_flush3 : std_logic_vector(31 downto 0);
begin
    s_flush1 <= (OTHERS => '0') WHEN iFlush = '1' ELSE currentPC;
    s_flush2 <= (OTHERS => '0') WHEN iFlush = '1' ELSE pcPlus4;
    s_flush3 <= (OTHERS => '0') WHEN iFlush = '1' ELSE instruction;


    IF_ID_Register: register_N
        generic map(N => N)
        port map(
            iCLK                => iCLK,
            iRST                => iRST,
            iWE                 => iWE,
            iD(95 downto 64)    => s_flush1,
            iD(63 downto 32)    => s_flush2,
            iD(31 downto 0)     => s_flush3,
            oQ(95 downto 64)    => oCurrentPC,
            oQ(63 downto 32)    => oPCPlus4,
            oQ(31 downto 0)     => oInstruction
        );
end structual;
