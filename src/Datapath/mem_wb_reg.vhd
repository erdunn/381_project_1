-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- MEM_WB_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the MEM/WB Register.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity mem_wb_reg is
    generic(N : integer := 73);
    port(
        iCLK       : in std_logic;
        iRST       : in std_logic;
        iWE        : in std_logic;
        control    : in std_logic_vector(3 downto 0); -- Stores Control Signals
        regDst     : in std_logic_vector(4 downto 0); -- Stores Destination Register Address
        memRead    : in std_logic_vector(31 downto 0); -- Stores Data Read from Memory
        ALURes     : in std_logic_vector(31 downto 0); -- Stores Result of ALU
        oControl   : out std_logic_vector(3 downto 0);
        oRegDst    : out std_logic_vector(4 downto 0);
        oMemRead   : out std_logic_vector(31 downto 0);
        oALURes    : out std_logic_vector(31 downto 0));
end mem_wb_reg;

architecture structual of mem_wb_reg is

    component register_N is
        generic(N : integer := 73);
        port(iCLK       : in std_logic;
             iRST       : in std_logic;
             iWE        : in std_logic;
             iD         : in std_logic_vector(N-1 downto 0);
             oQ         : out std_logic_vector(N-1 downto 0));
    end component;

        signal s_stall_2b : std_logic_vector(2 downto 0);
        signal s_stall_5b : std_logic_vector(4 downto 0);
        signal s_stall_32b_1,s_stall_32b_2: std_logic_vector(31 downto 0);
begin
    --s_stall_2b <=(others => '0') when flush = '1' else control;
    --s_stall_5b <=(others => '0') when flush = '1' else regDst;
    --s_stall_32b_1 <=(others => '0') when flush = '1' else memRead;
    --s_stall_32b_2 <=(others => '0') when flush = '1' else ALURes;
        MEM_WB_Register: register_N
        port map(
            iCLK       => iCLK,
            iRST       => iRST,
            iWE        => iWE,
            iD(72 downto 69) =>control,
            iD(68 downto 64) => regDst,
            iD(63 downto 32) =>memRead,
            iD(31 downto 0)  =>ALURes,
            oQ(72 downto 69) => oControl,
            oQ(68 downto 64) => oRegDst,
            oQ(63 downto 32) => oMemRead,
            oQ(31 downto 0)  => oALURes);
end structual;

