-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- mux4t1.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a 4 to 1 mux.
-------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mux4t1 is
    port (
        sel     : in std_logic_vector(1 downto 0);
        in0     : in std_logic_vector(31 downto 0);
        in1     : in std_logic_vector(31 downto 0);
        in2     : in std_logic_vector(31 downto 0);
        in3     : in std_logic_vector(31 downto 0);
        muxout  : out std_logic_vector(31 downto 0)
    );
end mux4t1;

architecture Behavioral of mux4t1 is
begin
    muxout <= in0 when sel = "00" else
              in1 when sel = "01" else
              in2 when sel = "10" else
              in3; -- unreachable
end Behavioral;
