-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- register.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a register.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity register_N is
    generic(N : integer := 32);
    port(iCLK       : in std_logic;
         iRST       : in std_logic;
         iWE        : in std_logic;
         iD         : in std_logic_vector(N-1 downto 0);
         oQ         : out std_logic_vector(N-1 downto 0));
end register_N;

architecture structual of register_N is 

    component dffg is
        port(i_CLK        : in std_logic;
             i_RST        : in std_logic;
             i_WE         : in std_logic;
             i_D          : in std_logic;
             o_Q          : out std_logic);
    end component;

begin

    G_NBit_DFF: for i in 0 to N-1 generate
        DFF: dffg port map(
            i_CLK   => iCLK,
            i_RST   => iRST,
            i_WE    => iWE,
            i_D     => iD(i),
            o_Q     => oQ(i));
    end generate G_NBit_DFF;

end structual;