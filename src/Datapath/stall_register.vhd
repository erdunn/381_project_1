-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- id_ex_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the ID/EX Register.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

-- Needs Register Reads, Register Addresses, Sign Extend, Control Signals, and PC+4
entity stall_register is 
    generic(N : integer := 76);
    port(
        iCLK        : in std_logic; -- Clock
        iRST        : in std_logic; -- Reset
        iWE         : in std_logic; -- Write Enable
        flush       : in std_logic;
        stall       : in std_logic;
        instAddr: IN STD_LOGIC_VECTOR(31 downto 0);
        inst        : in std_logic_vector(31 downto 0);
        opcode       : in std_logic_vector(5 downto 0); -- Shamt value for the ALU. 
        funct  : in std_logic_vector(5 downto 0);
        oInstAddr:OUT STD_LOGIC_VECTOR(31 downto 0);
        oinst        : out std_logic_vector(31 downto 0);
        oOpcode     : out std_logic_vector(5 downto 0);
        ofunct  : out std_logic_vector(5 downto 0));
end stall_register;

architecture structual of stall_register is
    
    component register_N is
        generic(N : integer := 76);
        port(iCLK       : in std_logic;
             iRST       : in std_logic;
             iWE        : in std_logic;
             iD         : in std_logic_vector(N-1 downto 0);
             oQ         : out std_logic_vector(N-1 downto 0));
    end component;   
     
    signal stall_reg_flush_1,stall_reg_flush_2,stall_reg_flush_3,stall_reg_flush_4 : std_logic_vector(5 downto 0);

begin
    stall_reg_flush_1 <= (others => '0') when flush = '1' else opcode;
    stall_reg_flush_2 <= (others => '0') when flush = '1' else funct;
    stall_reg_flush_3 <= (others => '0') when flush = '1' else oOpcode;
    stall_reg_flush_4 <= (others => '0') when flush = '1' else ofunct;

    
    stall_reg: register_N
    port map(
        iCLK       => iCLK,
        iRST       => iRST,
        iWE        => iWE,
        iD(75 downto 44) => instAddr,
        iD(43 downto 12) => inst,
        iD(11 downto 6) =>opcode,
        iD(5 downto 0)  =>funct,
        oQ(75 downto 44) => oInstAddr,
        oQ(43 downto 12) => oinst,
        oQ(11 downto 6) => oOpcode,
        oQ(5 downto 0)  => ofunct);







    -- STALL_REG: register_N -- TODO, but I think I fixed it.
    --     port map(
    --         iCLK    => iCLK,
    --         iRST    => iRST,
    --         iWE     => iWE,
    --         iD(11 downto 6)                 => stall_reg_flush_1,
    --         iD(5 downto 0)                  =>stall_reg_flush_2, 
    --         oQ(11 downto 6)                 => stall_reg_flush_3,
    --         oQ(5 downto 0)                  => stall_reg_flush_4);
end structual;
