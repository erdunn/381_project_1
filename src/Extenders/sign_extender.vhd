library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sign_extender is
    generic (
        INPUT_WIDTH : natural := 16
    );
    port (
        inputs : in std_logic_vector(INPUT_WIDTH - 1 downto 0);
        outputs : out std_logic_vector(31 downto 0)
    );
end sign_extender;

architecture rtl of sign_extender is
begin
    process(inputs)
    begin
        outputs(31 downto INPUT_WIDTH) <= (others => inputs(INPUT_WIDTH - 1));
        outputs(INPUT_WIDTH - 1 downto 0) <= inputs;
    end process;
end rtl;

