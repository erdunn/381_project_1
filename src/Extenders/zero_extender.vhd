library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity zero_extender is
    generic (
        INPUT_WIDTH : natural := 16
    );
    port (
        inputs : in std_logic_vector(INPUT_WIDTH - 1 downto 0);
        outputs : out std_logic_vector(31 downto 0)
    );
end zero_extender;

architecture rtl of zero_extender is
begin
    process(inputs)
    begin
        outputs(31 downto INPUT_WIDTH) <= (others => '0');
        outputs(INPUT_WIDTH - 1 downto 0) <= inputs;
    end process;
end rtl;

