library IEEE;	
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_STD.all;
entity fetch is
	-- generic(N : integer := 32); 
	port(iCLK		: in std_logic;   --clock
 	     iRST		: in std_logic;   --reset
	    i_A		: in std_logic_vector(31 downto 0); -- +4
		o_A			: out std_logic_vector(31 downto 0);   --input PC address
		i_B			: in std_logic_vector(29 downto 0); -- hacky value for jump/branch calculation
		o_B			: out std_logic_vector(31 downto 0) -- hacky output value.
	    --  i_signExt		: in std_logic_vector(N-1 downto 0);   --sign extension from mem
	    --  i_mem		: in std_logic_vector(25 downto 0);  --gives value from mem with 26 bits
	    --  i_zero		: in std_logic;   --zero from ALU
	    --  i_jump		: in std_logic;   --jump input 1 or 0
	    --  i_branch		: in std_logic;   --branch control
	    --  i_enable		: in std_logic;   --enable from register(PC) or not
		--  o_plusFour		: out std_logic_vector(N-1 downto 0);   --sign extension from mem
		--  i_ALUOP	: in std_logic_vector(4 downto 0);
		--  i_jr		: in std_logic;   --jump input 1 or 0
		--  i_jrdat		: in std_logic_vector(N-1 downto 0);   --sign extension from mem
	    --  o_A		: out std_logic_vector(N-1 downto 0)
		);   --output address from program counter
end fetch;
		

architecture Behavioral of Fetch is
	
	-- component mux2t1_N is
  	-- generic(N : integer := 32);
  	-- port(i_S          : in std_logic;
    --    	     i_D0         : in std_logic_vector(N-1 downto 0);
    --    	     i_D1         : in std_logic_vector(N-1 downto 0);
    --    	     o_O          : out std_logic_vector(N-1 downto 0));

	-- end component;

	
	-- component full_adder_N is
	-- generic(N : integer := 32);
	-- port(i_Xv	: in std_logic_vector(N-1 downto 0);
	--      i_Yv	: in std_logic_vector(N-1 downto 0);
	--      i_Cv	: in std_logic;
	--      o_Sv	: out std_logic_vector(N-1 downto 0);
	--      o_Cv	: out std_logic);
	-- end component;


	-- component andg2 is

  	-- port(i_A          : in std_logic;
    --    	i_B          : in std_logic;
    --    	o_F          : out std_logic);

	-- end component;

	  

signal plusFour 		: std_logic_vector(31 downto 0); --+4 used in pc+4
signal branch_target :  STD_LOGIC_VECTOR (31 downto 0); -- The target address if a branch is taken
-- signal jump_target :  STD_LOGIC_VECTOR (31 downto 0); -- The target address if a jump is taken
-- signal vhdlSucks	:  STD_LOGIC_VECTOR (31 downto 0);
-- signal temp2	:  STD_LOGIC_VECTOR (31 downto 0);
-- signal temp  : STD_LOGIC_VECTOR (31 downto 0);



begin
	plusFour <= i_A + X"00000004";
    -- temp <= i_signExt(29 downto 0) & "00";

	branch_target <= i_B & "00";

	-- add1: full_adder_N
	-- port map(
	-- 	i_Xv	=> plusFour,
	--      i_Yv	=> temp,
	--      i_Cv	=> '0',
	--      o_Sv	=> branch_target,
	--      o_Cv	=> open
	-- );
	-- 	--plusFour <= i_A;
	-- 		jump_target <= plusFour(31 downto 28) & (i_mem(25 downto 0) & "00");


		-- if i_branch = '1' and i_zero = '0' and i_ALUOP = "01001" then -- BNE
		-- 	vhdlSucks <= branch_target;
		-- 	o_A <= vhdlSucks;
		-- elsif i_branch = '1' and i_zero = '1' and i_ALUOP = "01010" then -- BEQ
		-- 	vhdlSucks <= branch_target;
		-- 	o_A <= vhdlSucks;
		-- elsif i_jump = '1' then
		-- 	vhdlSucks <= jump_target;
		-- 	--o_A <= vhdlSucks;
		-- else
		-- vhdlSucks <= plusFour;
		-- end if;

		-- with i_jump & i_branch & i_zero & (i_ALUOP) select
		-- 	vhdlSucks <= branch_target when "01001001", --bne
		-- 				branch_target when "01101010",				--beq
		-- 				jump_target when "101-----",
		-- 				plusFour when others;

		
			
		
	-- 	vhdlSucks <= branch_target when i_jump = '0' and i_branch = '1' and i_zero = '1' and i_ALUOP = "01010" else
	-- 				i_jrdat when i_jr = '1' else -- bne
	-- 				 jump_target when i_jump = '1' else
	-- 				 branch_target when i_jump = '0' and i_branch = '1' and i_zero = '0' and i_ALUOP = "01001"else
	-- 				 plusFour;




	o_B <= branch_target;
	-- o_A <= vhdlSucks;
	o_A <= plusFour;
end Behavioral;

