library IEEE;
use IEEE.std_logic_1164.all;

entity full_adder is
	port(i_X : in std_logic;
	     i_Y : in std_logic;
	     i_C : in std_logic;
	     o_S : out std_logic;
	     o_C : out std_logic);
end full_adder;


architecture structure of full_adder is

component xorg2
	port(i_A	: in std_logic;
	     i_B	: in std_logic;
	     o_F	: out std_logic);
end component;

component andg2
	port(i_A          : in std_logic;
             i_B          : in std_logic;
             o_F          : out std_logic);
end component;

component org2
	port(i_A          : in std_logic;
             i_B          : in std_logic;
             o_F          : out std_logic);
end component;

signal s_T0	: std_logic;
signal s_T1	: std_logic;
signal s_T2	: std_logic;

begin

m_xorGate1: xorg2
	port MAP(i_A	=> i_X,
		 i_B	=> i_Y,
		 o_F	=> s_T0);

m_xorGate2: xorg2
	port MAP(i_A	=> s_T0,
		 i_B	=> i_C,
		 o_F	=> o_S);

m_andGate1: andg2
	port MAP(i_A	=> i_X,
		 i_B	=> i_Y,
		 o_F	=> s_T1);

m_andGate2: andg2
	port MAP(i_A	=> s_T0,
		 i_B	=> i_C,
		 o_F	=> s_T2);

m_orGate1: org2
	port MAP(i_A	=> s_T1,
		 i_B	=> s_T2,
		 o_F	=> o_C);

end structure;
