-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- if_id_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of the forwarding unit.
--              Page 306 in the book
-------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;

entity forwarding_unit is
  port(
		EX_MEM_RegRd 	: in std_logic_vector(4 downto 0); -- EX/MEM register destination address
		ID_EX_RegRs 	: in std_logic_vector(4 downto 0); -- ID/EX register source address
		ID_EX_RegRt 	: in std_logic_vector(4 downto 0); -- ID/EX register source address
		MEM_WB_RegRd	: in std_logic_vector(4 downto 0); -- MEM/WB register destination address
		EX_MEM_RegWrite	: in std_logic; -- EX/MEM register write control signal
		MEM_WB_RegWrite	: in std_logic; -- MEM/WB register write control signal
		Forward_A		: out std_logic_vector(1 downto 0); -- 2-bit signal to select input for A operand of ALU
		Forward_B		: out std_logic_vector(1 downto 0)); -- 2-bit signal to select input for B operand of ALU

end forwarding_unit;

architecture structure of forwarding_unit is

begin

	-- Forward A
	Forward_A <= 	"00" when (EX_MEM_RegWrite = '0') else -- No Hazard
					"01" when ((MEM_WB_RegWrite = '1') and
							not(MEM_WB_RegRd = "00000") and
							not((EX_MEM_RegWrite = '1') and not(EX_MEM_RegRd = "00000") and not(EX_MEM_RegRd = ID_EX_RegRs)) and
							(MEM_WB_RegRd = ID_EX_RegRs)) else -- MEM Hazard
					"10" when ((EX_MEM_RegWrite = '1')
							and not(EX_MEM_RegRd = "00000") 
							and (EX_MEM_RegRd = ID_EX_RegRs)) else -- EX Hazard
					"00"; -- No Hazard

	-- Forward B
	Forward_B <= "00" when (EX_MEM_RegWrite = '0') else
					"01" when ((MEM_WB_RegWrite = '1') and
							not(MEM_WB_RegRd = "00000") and
							not((EX_MEM_RegWrite = '1') and not(EX_MEM_RegRd = "00000") and not(EX_MEM_RegRd = ID_EX_RegRt)) and
							(MEM_WB_RegRd = ID_EX_RegRt)) else -- MEM Hazard
					"10" when ((EX_MEM_RegWrite = '1')
							and not(EX_MEM_RegRd = "00000")
							and (EX_MEM_RegRd = ID_EX_RegRt)) else -- EX Hazard
					"00"; -- No Hazard
end structure;

