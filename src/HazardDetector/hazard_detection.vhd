-- -------------------------------------------------------------------------
-- -- Alex Bashara
-- -- Department of Electrical and Computer Engineering
-- -- Iowa State University
-- -------------------------------------------------------------------------
-- -- id_ex_reg.vhd
-- -------------------------------------------------------------------------
-- -- DESCRIPTION: This file contains an implementation of the hazard detection unit.
-- -- 				Book page 314
-- -------------------------------------------------------------------------

-- library IEEE;
-- use IEEE.std_logic_1164.all;

-- entity hazard_detection is
-- port(
-- 		ID_EX_MEMRead : in std_logic; -- 1 if ID/EX is a load instruction
-- 		ID_EX_RegRt : in std_logic_vector(4 downto 0); -- register number of rt
-- 		IF_ID_RegRs : in std_logic_vector(4 downto 0); -- register number of rs
-- 		IF_ID_RegRt : in std_logic_vector(4 downto 0); -- register number of rt
-- 		ControlMux : out std_logic; -- Controls MUX infont of ID/EX Register (See Page 316)
-- 		stall : out std_logic; -- 1 if stall is needed
-- 		PCwrite : out std_logic; -- 1 if PCwrite is needed
-- 		);

-- end hazard_detection;

-- architecture structure of hazard_detection is
-- begin
-- 	if ID_EX_MEMRead = '1' and ((ID_EX_RegRt = IF_ID_RegRs) or (ID/EX_RegRt = IF_ID_RegRt)) then
-- 		ControlMux = '1';
-- 		stall = '1';
-- 		PCwrite = '1';
-- 	else
-- 		ControlMux = '0';
-- 		stall = '0';
-- 		PCwrite = '0';
-- 	end if;

-- end structure;