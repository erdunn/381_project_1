-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- addsub.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an N-bit adder/subtractor.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;

entity addsub is 
    generic(N   : integer := 32);
    port(ALUSrc     : in std_logic;
         nAdd_Sub   : in std_logic;
         iA         : in std_logic_vector(N-1 downto 0);
         iB         : in std_logic_vector(N-1 downto 0);
         imm        : in std_logic_vector(N-1 downto 0);
         sum        : out std_logic_vector(N-1 downto 0);
         carry      : out std_logic);

end addsub;

architecture structural of addsub is 

    component myadder
        port(iC     : in std_logic;
             iA     : in std_logic_vector(N-1 downto 0);
             iB     : in std_logic_vector(N-1 downto 0);
             oS     : out std_logic_vector(N-1 downto 0);
             oC     : out std_logic);
    end component;

    component mux2t1_N
        port(i_S        : in std_logic;
             i_D0       : in std_logic_vector(N-1 downto 0);
             i_D1       : in std_logic_vector(N-1 downto 0);
             o_O        : out std_logic_vector(N-1 downto 0));
    end component;
        
    component onescomp
        port(i_in       : in  std_logic_vector(N - 1 downto 0);
             o_out      : out std_logic_vector(N - 1 downto 0));
    end component;


    signal BSel     : std_logic_vector(N-1 downto 0);
    signal comp     : std_logic_vector(N-1 downto 0);
    signal AdSbSel  : std_logic_vector(N-1 downto 0);

    begin

        bMux: mux2t1_N
            port map(i_S        => ALUSrc,
                     i_D0       => iB,
                     i_D1       => imm,
                     o_O        => BSel);
        b_inv: onescomp
            port map(i_in       => BSel,
                     o_out      => comp);

        ASMux: mux2t1_N
            port map(i_S        => nAdd_Sub,
                     i_D0       => BSel,
                     i_D1       => comp,
                     o_O        => AdSbSel);

        Math: myadder
            port map(iC         => nAdd_Sub,
                     iA         => iA,
                     iB         => AdSbSel,
                     oS         => sum,
                     oC         => carry);
end structural;
