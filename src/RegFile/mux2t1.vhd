-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- mux2t1.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a 2 to 1 Mux
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity mux2t1 is 

    port(i_D0         : in std_logic;
         i_D1         : in std_logic;
         i_S          : in std_logic;
         o_O          : out std_logic);

end mux2t1;

architecture dataflow of mux2t1 is  
begin
    process (i_D0, i_D1, i_S)
    begin
        if i_S = '0' then
            o_O <= i_D0;
        else
            o_O <= i_D1;
        end if;
    end process;
end dataflow;
