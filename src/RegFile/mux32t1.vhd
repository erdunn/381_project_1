-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- mux32t1.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a 32 to 1 Mux
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity mux32t1 is 

    port(sel        : in std_logic_vector(4 downto 0);
         iD0        : in std_logic_vector(31 downto 0);
         iD1        : in std_logic_vector(31 downto 0);
         iD2        : in std_logic_vector(31 downto 0);
         iD3        : in std_logic_vector(31 downto 0);
         iD4        : in std_logic_vector(31 downto 0);
         iD5        : in std_logic_vector(31 downto 0);
         iD6        : in std_logic_vector(31 downto 0);
         iD7        : in std_logic_vector(31 downto 0);
         iD8        : in std_logic_vector(31 downto 0);
         iD9        : in std_logic_vector(31 downto 0);
         iD10       : in std_logic_vector(31 downto 0);
         iD11       : in std_logic_vector(31 downto 0);
         iD12       : in std_logic_vector(31 downto 0);
         iD13       : in std_logic_vector(31 downto 0);
         iD14       : in std_logic_vector(31 downto 0);
         iD15       : in std_logic_vector(31 downto 0);
         iD16       : in std_logic_vector(31 downto 0);
         iD17       : in std_logic_vector(31 downto 0);
         iD18       : in std_logic_vector(31 downto 0);
         iD19       : in std_logic_vector(31 downto 0);
         iD20       : in std_logic_vector(31 downto 0);
         iD21       : in std_logic_vector(31 downto 0);
         iD22       : in std_logic_vector(31 downto 0);
         iD23       : in std_logic_vector(31 downto 0);
         iD24       : in std_logic_vector(31 downto 0);
         iD25       : in std_logic_vector(31 downto 0);
         iD26       : in std_logic_vector(31 downto 0);
         iD27       : in std_logic_vector(31 downto 0);
         iD28       : in std_logic_vector(31 downto 0);
         iD29       : in std_logic_vector(31 downto 0);
         iD30       : in std_logic_vector(31 downto 0);
         iD31       : in std_logic_vector(31 downto 0);
         val        : out std_logic_vector(31 downto 0));

end mux32t1;

architecture dataflow of mux32t1 is 
begin
    process (sel, iD0, iD1, iD2, iD3, iD4, iD5, iD6, iD7, iD8, iD9, iD10, iD11, iD12, iD13, iD14, iD15, iD16,
             iD17, iD18, iD19, iD20, iD21, iD22, iD23, iD24, iD25, iD26, iD27, iD28, iD29, iD30, iD31)
    begin
        case sel is
            when "00000" => val <= iD0;
            when "00001" => val <= iD1;
            when "00010" => val <= iD2;
            when "00011" => val <= iD3;
            when "00100" => val <= iD4;
            when "00101" => val <= iD5;
            when "00110" => val <= iD6;
            when "00111" => val <= iD7;
            when "01000" => val <= iD8;
            when "01001" => val <= iD9;
            when "01010" => val <= iD10;
            when "01011" => val <= iD11;
            when "01100" => val <= iD12;
            when "01101" => val <= iD13;
            when "01110" => val <= iD14;
            when "01111" => val <= iD15;
            when "10000" => val <= iD16;
            when "10001" => val <= iD17;
            when "10010" => val <= iD18;
            when "10011" => val <= iD19;
            when "10100" => val <= iD20;
            when "10101" => val <= iD21;
            when "10110" => val <= iD22;
            when "10111" => val <= iD23;
            when "11000" => val <= iD24;
            when "11001" => val <= iD25;
            when "11010" => val <= iD26;
            when "11011" => val <= iD27;
            when "11100" => val <= iD28;
            when "11101" => val <= iD29;
            when "11110" => val <= iD30;
            when "11111" => val <= iD31;
            when others => val <= "00000000000000000000000000000000";
        end case;
    end process;
end dataflow;