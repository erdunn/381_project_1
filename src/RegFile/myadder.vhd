-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- myadder.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an N-bit adder.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;

entity myadder is 
    generic(N : integer := 32);
    port(iC     : in std_logic;
         iA     : in std_logic_vector(N-1 downto 0);
         iB     : in std_logic_vector(N-1 downto 0);
         oS     : out std_logic_vector(N-1 downto 0);
         oC     : out std_logic);
end myadder;

architecture structural of myadder is 

    component xorg2
        port(i_A          : in std_logic;
             i_B          : in std_logic;
             o_F          : out std_logic);
    end component;

    component andg2 
        port(i_A          : in std_logic;
             i_B          : in std_logic;
             o_F          : out std_logic);
    end component;

    component org2
        port(i_A          : in std_logic;
             i_B          : in std_logic;
             o_F          : out std_logic);
    end component;

    signal carry : std_logic_vector(N downto 0); -- Stores Carry Values
    signal sA : std_logic_vector(N downto 0); -- Stores A XOR B
    signal sB : std_logic_vector(N downto 0); -- Stores (A XOR B) && Cin
    signal sC : std_logic_vector(N downto 0); -- Stores A and B

    begin

    carry(0) <= iC;

    G_NBit_Adder: for i in 0 to N-1 generate

    --Sum Logic
    XOR_Sum: xorg2
        port map(i_A        => iA(i),
                 i_B        => iB(i),
                 o_F        => sA(i));

    XOR_Carry: xorg2
        port map(i_A        => sA(i),
                 i_B        => carry(i),
                 o_F        => oS(i));

    -- Carry Logic
    AND_Carry: andg2
        port map(i_A        => sA(i),
                 i_B        => carry(i),
                 o_F        => sB(i));

    AND_In: andg2
        port map(i_A        => iA(i),
                 i_B        => iB(i),
                 o_F        => sC(i));

    OR_Carry: org2
        port map(i_A        => sB(i),
                 i_B        => sC(i),
                 o_F        => carry(i+1));

    end generate G_NBit_Adder;

    oC <= carry(N);

    end structural;