-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- onescomp.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a ones complimentor
-------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity onescomp is

  generic (N : integer := 32);
  port (
    i_in  : in  STD_LOGIC_VECTOR(N - 1 downto 0);
    o_out : out STD_LOGIC_VECTOR(N - 1 downto 0));

end entity onescomp;

architecture structural of onescomp is

component invg
  port(i_A  : in std_logic;
      o_F   : out std_logic);
end component;

begin

  G_NBit_onescomp: for i in 0 to N - 1 generate
  
  inv: invg 
      port map(
        i_A => i_in(i),
        o_F => o_out(i)); 
  end generate G_NBit_onescomp;

end structural;