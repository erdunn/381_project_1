-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- register_file.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an implementation of a register file
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity register_file is 
    
    port(clk        : in std_logic; 
         rst        : in std_logic; -- Reset
         WEn        : in std_logic; -- Write Enable
         WriteR     : in std_logic_vector(4 downto 0); -- Write Select
         Read1      : in std_logic_vector(4 downto 0); -- Read 1 Select
         Read2      : in std_logic_vector(4 downto 0); -- Read 2 Select
         data       : in std_logic_vector(31 downto 0); -- Write Data
         oRead1     : out std_logic_vector(31 downto 0); -- Data from Register 1
         oRead2     : out std_logic_vector(31 downto 0)); -- Data from Register 2

end register_file;

architecture structural of register_file is

    component mux32t1
        port(sel        : in std_logic_vector(4 downto 0);
             iD0        : in std_logic_vector(31 downto 0);
             iD1        : in std_logic_vector(31 downto 0);
             iD2        : in std_logic_vector(31 downto 0);
             iD3        : in std_logic_vector(31 downto 0);
             iD4        : in std_logic_vector(31 downto 0);
             iD5        : in std_logic_vector(31 downto 0);
             iD6        : in std_logic_vector(31 downto 0);
             iD7        : in std_logic_vector(31 downto 0);
             iD8        : in std_logic_vector(31 downto 0);
             iD9        : in std_logic_vector(31 downto 0);
             iD10       : in std_logic_vector(31 downto 0);
             iD11       : in std_logic_vector(31 downto 0);
             iD12       : in std_logic_vector(31 downto 0);
             iD13       : in std_logic_vector(31 downto 0);
             iD14       : in std_logic_vector(31 downto 0);
             iD15       : in std_logic_vector(31 downto 0);
             iD16       : in std_logic_vector(31 downto 0);
             iD17       : in std_logic_vector(31 downto 0);
             iD18       : in std_logic_vector(31 downto 0);
             iD19       : in std_logic_vector(31 downto 0);
             iD20       : in std_logic_vector(31 downto 0);
             iD21       : in std_logic_vector(31 downto 0);
             iD22       : in std_logic_vector(31 downto 0);
             iD23       : in std_logic_vector(31 downto 0);
             iD24       : in std_logic_vector(31 downto 0);
             iD25       : in std_logic_vector(31 downto 0);
             iD26       : in std_logic_vector(31 downto 0);
             iD27       : in std_logic_vector(31 downto 0);
             iD28       : in std_logic_vector(31 downto 0);
             iD29       : in std_logic_vector(31 downto 0);
             iD30       : in std_logic_vector(31 downto 0);
             iD31       : in std_logic_vector(31 downto 0);
             val        : out std_logic_vector(31 downto 0));
    end component;

    component decoder
        port(EN     : in std_logic;
             I      : in std_logic_vector(4 downto 0);
             O      : out std_logic_vector(31 downto 0));
    end component;

    component register_N
        generic(N : integer := 32);
        port(iCLK       : in std_logic;
             iRST       : in std_logic;
             iWE        : in std_logic;
             iD         : in std_logic_vector(N-1 downto 0);
             oQ         : out std_logic_vector(N-1 downto 0));
    end component;

    type REG is array (31 downto 0) of std_logic_vector(31 downto 0);
    signal QReg         : REG;
    signal odecoder     : std_logic_vector(31 downto 0);
    signal load_and     : std_logic;
    signal nClk         : std_logic;

begin

    nClk <= not clk;

    reg_select: decoder
        port MAP(En     => WEn,
                 I      => WriteR,
                 O      => odecoder);

    reg_0: register_N
        port MAP(iCLK   => clk,
                 iRST   => '1',
                 iWE    => '0',
                 iD     => (others => '0'),
                 oQ     => QReg(0));

    gen_reg: for i in 1 to 31 generate
        load_and <= odecoder(31-i) and WEn;
        register_N_i : register_N
            port MAP(iCLK   => nClk,
                     iRST   => rst,
                     iWE    => odecoder(i),
                     iD     => data,
                     oQ     => QReg(i));
    end generate;

    reg1_read: mux32t1
        port MAP(sel        => Read1,
                 iD0        => QReg(0),
                 iD1        => QReg(1),
                 iD2        => QReg(2),
                 iD3        => QReg(3),
                 iD4        => QReg(4),
                 iD5        => QReg(5),
                 iD6        => QReg(6),
                 iD7        => QReg(7),
                 iD8        => QReg(8),
                 iD9        => QReg(9),
                 iD10       => QReg(10),
                 iD11       => QReg(11),
                 iD12       => QReg(12),
                 iD13       => QReg(13),
                 iD14       => QReg(14),
                 iD15       => QReg(15),
                 iD16       => QReg(16),
                 iD17       => QReg(17),
                 iD18       => QReg(18),
                 iD19       => QReg(19),
                 iD20       => QReg(20),
                 iD21       => QReg(21),
                 iD22       => QReg(22),
                 iD23       => QReg(23),
                 iD24       => QReg(24),
                 iD25       => QReg(25),
                 iD26       => QReg(26),
                 iD27       => QReg(27),
                 iD28       => QReg(28),
                 iD29       => QReg(29),
                 iD30       => QReg(30),
                 iD31       => QReg(31),
                 val        => oRead1);

    reg2_read: mux32t1
        port MAP(sel        => Read2,
                 iD0        => QReg(0),
                 iD1        => QReg(1),
                 iD2        => QReg(2),
                 iD3        => QReg(3),
                 iD4        => QReg(4),
                 iD5        => QReg(5),
                 iD6        => QReg(6),
                 iD7        => QReg(7),
                 iD8        => QReg(8),
                 iD9        => QReg(9),
                 iD10       => QReg(10),
                 iD11       => QReg(11),
                 iD12       => QReg(12),
                 iD13       => QReg(13),
                 iD14       => QReg(14),
                 iD15       => QReg(15),
                 iD16       => QReg(16),
                 iD17       => QReg(17),
                 iD18       => QReg(18),
                 iD19       => QReg(19),
                 iD20       => QReg(20),
                 iD21       => QReg(21),
                 iD22       => QReg(22),
                 iD23       => QReg(23),
                 iD24       => QReg(24),
                 iD25       => QReg(25),
                 iD26       => QReg(26),
                 iD27       => QReg(27),
                 iD28       => QReg(28),
                 iD29       => QReg(29),
                 iD30       => QReg(30),
                 iD31       => QReg(31),
                 val        => oRead2);

end structural;