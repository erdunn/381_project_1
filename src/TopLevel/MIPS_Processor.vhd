------------------------------------------------------------------------
-- Henry Duwe
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- MIPS_Processor.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a MIPS_Processor  
-- implementation.

-- 01/29/2019 by H3::Design created.
-- 10/18/23 by AB: Added Functional Blocks
-------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.MIPS_types.all;

entity MIPS_Processor is
  generic(N : integer := DATA_WIDTH);
  port(iCLK            : in std_logic;
       iRST            : in std_logic;
       iInstLd         : in std_logic;
       iInstAddr       : in std_logic_vector(N-1 downto 0);
       iInstExt        : in std_logic_vector(N-1 downto 0);
       oALUOut         : out std_logic_vector(N-1 downto 0)); -- TODO: Hook this up to the output of the ALU. It is important for synthesis that you have this output that can effectively be impacted by all other components so they are not optimized away.

end  MIPS_Processor;


architecture structure of MIPS_Processor is

  component mem is
    generic(ADDR_WIDTH : integer;
            DATA_WIDTH : integer);
    port(
          clk          : in std_logic;
          addr         : in std_logic_vector((ADDR_WIDTH-1) downto 0);
          data         : in std_logic_vector((DATA_WIDTH-1) downto 0);
          we           : in std_logic := '1';
          q            : out std_logic_vector((DATA_WIDTH -1) downto 0));
    end component;

    component if_id_reg is 
    generic(N : integer := 96);
    port(
        iCLK        : in std_logic; -- Clock
        iRST        : in std_logic; -- Reset
        iWE         : in std_logic; -- Write Enable
        iFlush      : in std_logic;
        currentPC   : in std_logic_vector(31 downto 0); -- Holds Current PC Address
        pcPlus4     : in std_logic_vector(31 downto 0); -- Holds PC+4 Address
        instruction : in std_logic_vector(31 downto 0); -- Holds Instruction
        oCurrentPC  : out std_logic_vector(31 downto 0); -- Outputs Current PC Address
        oPCPlus4    : out std_logic_vector(31 downto 0); -- Outputs PC+4 Address
        oInstruction: out std_logic_vector(31 downto 0)-- Outputs Instruction
); -- Outputs Flush
end component;


component id_ex_reg is 
    generic(N : integer := 164);
    port(
        iCLK        : in std_logic; -- Clock
        iRST        : in std_logic; -- Reset
        iWE         : in std_logic; -- Write Enable
        regdst      : in std_logic;
        iFlush      : in std_logic;
        shamt       : in std_logic_vector(4 downto 0); -- Shamt value for the ALU. 
        control     : in std_logic_vector(14 downto 0); -- Control Signals
        pcPlus4     : in std_logic_vector(31 downto 0); -- PC+4
        readData1   : in std_logic_vector(31 downto 0); -- Read Data 1 from Register File
        readData2   : in std_logic_vector(31 downto 0); -- Read Data 2 from Register File
        signExtend  : in std_logic_vector(31 downto 0); -- Sign Extended Immediate
        rsAddress   : in std_logic_vector(4 downto 0); -- Register Address 1
        rtAddress   : in std_logic_vector(4 downto 0); -- Register Address 2
        rdAddress   : in std_logic_vector(4 downto 0); -- Register Address 3
        oregdst      : out std_logic;
        oControl    : out std_logic_vector(14 downto 0); 
        oShamt       : out std_logic_vector(4 downto 0); -- Shamt value for the ALU. 
        oPcPlus4    : out std_logic_vector(31 downto 0);
        oReadData1  : out std_logic_vector(31 downto 0);
        oReadData2  : out std_logic_vector(31 downto 0);
        oSignExtend : out std_logic_vector(31 downto 0);
        oRsAddress  : out std_logic_vector(4 downto 0);
        oRtAddress  : out std_logic_vector(4 downto 0);
        oRdAddress  : out std_logic_vector(4 downto 0));
end component;


component ex_mem_reg is
  generic(N : integer := 110);
  port(
      iCLK        : in std_logic;
      iRST        : in std_logic;
      iWE         : in std_logic;
      control     : in std_logic_vector(8 downto 0);
      pcPlus4     : in std_logic_vector(31 downto 0);
      regDst      : in std_logic_vector(4 downto 0); -- Stores Destination Register Address
      ALURes      : in std_logic_vector(31 downto 0);
      readData2   : in std_logic_vector(31 downto 0);
      oControl    : out std_logic_vector(8 downto 0);
      oPCPlus4    : out std_logic_vector(31 downto 0);
      oRegDst     : out std_logic_vector(4 downto 0);
      oALURes     : out std_logic_vector(31 downto 0);
      oReadData2  : out std_logic_vector(31 downto 0));
end component;



component mem_wb_reg is
  generic(N : integer := 73);
  port(
      iCLK       : in std_logic;
      iRST       : in std_logic;
      iWE        : in std_logic;
      control    : in std_logic_vector(3 downto 0); -- Stores Control Signals
      regDst     : in std_logic_vector(4 downto 0); -- Stores Destination Register Address
      memRead    : in std_logic_vector(31 downto 0); -- Stores Data Read from Memory
      ALURes     : in std_logic_vector(31 downto 0); -- Stores Result of ALU
      oControl   : out std_logic_vector(3 downto 0);
      oRegDst    : out std_logic_vector(4 downto 0);
      oMemRead   : out std_logic_vector(31 downto 0);
      oALURes    : out std_logic_vector(31 downto 0));
end component;



component full_adder_N is
	generic(N : integer := 32);
	port(i_Xv	: in std_logic_vector(N-1 downto 0);
	     i_Yv 	: in std_logic_vector(N-1 downto 0);
	     i_Cv 	: in std_logic;
	     o_Sv 	: out std_logic_vector(N-1 downto 0);
	     o_Cv 	: out std_logic);
end component;









 
  -- Control Unit Component
  component controlunit is 
  Port(
    opcode              : in  std_logic_vector(5 downto 0); -- Takes in the opcode field of an instruction
    funct               : in  std_logic_vector(5 downto 0); -- Takes in the funct field of an instruction
    regDst              : out std_logic; -- Determines whether the destination register is rt or rd
    branch              : out std_logic; -- Determines whether the instruction is a branch
    jump                : out std_logic; -- Determines whether the instruction is a jump
    jr                  : out std_logic;
    jal                 : out std_logic;
    memRead             : out std_logic; -- Determines whether the instruction reads memory
    memToReg            : out std_logic; -- Determines whether the instruction writes memory to register
    ALUOp               : out std_logic_vector(4 downto 0); -- Determines the ALU operation
    memWrite            : out std_logic; -- Sets Memory write
    ALUSrc              : out std_logic; -- Determines the second ALU operand
    regWrite            : out std_logic; -- Determines whether the instruction writes to a register
    extender            : out std_logic; -- determines whether a value is zero extended or sign extended
    halt                : out std_logic); -- Determines whether the instruction is a halt
  end component;

  -- Extender Component
  component zero_extender is 
    generic(INPUT_WIDTH : integer);
    port (
      inputs               : in  std_logic_vector(INPUT_WIDTH-1 downto 0); -- Takes in the 16 bit immediate field of an instruction
      outputs              : out std_logic_vector(31 downto 0)); -- Outputs the 32 bit immediate field of an instruction
  end component;

  component sign_extender is
    generic (INPUT_WIDTH : integer);
    port (
      inputs               : in std_logic_vector(INPUT_WIDTH - 1 downto 0); -- **** Input and Output are keywords *****
      outputs              : out std_logic_vector(31 downto 0));
  end component;

  -- And Gate
  component andg2 is
    port(
      i_A                 : in std_logic;
      i_B                 : in std_logic;
      o_F                 : out std_logic);
  end component;

  -- Registerfile Component
  component register_file is
    port (
         clk        : in std_logic; 
         rst        : in std_logic; -- Reset
         WEn        : in std_logic; -- Write Enable
         WriteR     : in std_logic_vector(4 downto 0); -- Write Select
         Read1      : in std_logic_vector(4 downto 0); -- Read 1 Select
         Read2      : in std_logic_vector(4 downto 0); -- Read 2 Select
         data       : in std_logic_vector(31 downto 0); -- Write Data
         oRead1     : out std_logic_vector(31 downto 0); -- Data from Register 1
         oRead2     : out std_logic_vector(31 downto 0) -- Data from Register 2
         );
  end component;


  -- ALU Component
  component ALU is
    generic(N : integer := 32);
    port(
      i_A	                : in std_logic_vector(N-1 downto 0);
      i_B	                : in std_logic_vector(N-1 downto 0);
      i_ALUOP	            : in std_logic_vector(4 downto 0);
      i_shamt			        : in std_logic_vector(4 downto 0);
      o_F	                : out std_logic_vector(N-1 downto 0);
      o_Overflow	        : out std_logic;
      o_Zero	           	: out std_logic);
    end component;

  -- Barrel Shifter Component
  component bShifter is 
    generic(N : integer := 8);
    port(
      i_X                 : in STD_LOGIC_VECTOR(N-1 downto 0); -- Input data
      i_S0                : in STD_LOGIC;      -- Control signal S0 (0 for left, 1 for right)
      i_S1                : in STD_LOGIC;      -- Control signal S1 (0 for shift, 1 for rotate)
      o_Y                 : out STD_LOGIC_VECTOR(N-1 downto 0)); -- Output data
  end component;

  -- Fetch Component
  component fetch is 
    port(iCLK       		  : in std_logic;   --clock
 	     iRST	         	  : in std_logic;   --reset
	     i_A	          	  : in std_logic_vector(31 downto 0);   --input PC address
       i_B	          	  : in std_logic_vector(29 downto 0);   --input PC address
	     o_B		            : out std_logic_vector(31 downto 0);   --output address from program counter
	     o_A		            : out std_logic_vector(31 downto 0));   --output address from program counter
  end component;

  -- 32b Mux Component
  component mux_32x1 is 
    port(
      Inputs              : in std_logic_vector(31 downto 0);
      Selects             : in std_logic_vector(4 downto 0);
      Output              : out std_logic_vector(31 downto 0));
  end component;

  component mux2t1_N is 
    generic(N : integer);
    port (
      i_S                 : in std_logic;
      i_D0                : in std_logic_vector(N-1 downto 0);
      i_D1                : in std_logic_vector(N-1 downto 0);
      o_O                 : out std_logic_vector(N-1 downto 0));
  end component;

  component pc_reg is

		port(i_CLK        : in std_logic;     -- Clock input
			 i_RST        : in std_logic;     -- Reset input
			 i_WE         : in std_logic;     -- Write enable input
			 i_D          : in std_logic_vector(31 downto 0);     -- Data value input
			 o_Q          : out std_logic_vector(31 downto 0));   -- Data value output
	  
	  end component;

    -- Required data memory signals
  signal s_DMemWr       : std_logic; -- TODO: use this signal as the final active high data memory write enable signal
  signal s_DMemAddr     : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory address input
  signal s_DMemData     : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory data input
  signal s_DMemOut      : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the data memory output
 
  -- Required register file signals 
  signal s_RegWr        : std_logic; -- TODO: use this signal as the final active high write enable input to the register file
  signal s_RegWrAddr,s_rd_add_ex    : std_logic_vector(4 downto 0); -- TODO: use this signal as the final destination register address input
  signal s_RegWrData    : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory data input
  signal s_RegReadData1 : std_logic_vector(DATA_WIDTH - 1 downto 0); -- RegFile output 1
  --signal s_RegReadData2 : std_logic_vector(DATA_WIDTH - 1 downto 0); -- RegFile output 2

  -- Required instruction memory signals
  signal s_IMemAddr     : std_logic_vector(N-1 downto 0); -- Do not assign this signal, assign to s_NextInstAddr instead
  signal s_NextInstAddr : std_logic_vector(N-1 downto 0); -- TODO: use this signal as your intended final instruction memory address input.
  signal s_Inst,s_Inst_de         : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the instruction signal 

  -- Required halt signal -- for simulation
  signal s_Halt, s_halt_temp, s_regWr_temp        : std_logic;  -- TODO: this signal indicates to the simulation that intended program execution has completed. (Opcode: 01 0100)

  -- Required overflow signal -- for overflow exception detection
  signal s_Ovfl, s_ovfl_ex, s_ovfl_mem, s_extender         : std_logic;  -- TODO: this signal indicates an overflow exception would have been initiated

  --Josh's added signals (feel free to change if you find redundancy or errors)
  signal s_zeroToFetch  : std_logic; --zero signal output from alu that goes to fetch
  signal s_branch     	: std_logic; --branch output from control unit to fetch logic
  signal s_jump   	 	  : std_logic; --jump output from control to fetch logic
  signal s_stall, s_flush, s_stall_r, s_flush_r        : std_logic; -- stall the program, flush
  --signal s_signExtended : std_logic_vector(N-1 downto 0);

     -- TODO: You may add any additional signals or components your implementation 
  --       requires below this comment
  -- MUX control Signals
  signal s_regDst       : std_logic; -- MUX control signal for RegDst. 
  signal s_alusrc       : std_logic; -- MUX control signal for aluSRC.
  --MUX Output Signals
  signal s_ALU2,s_temp         : std_logic_vector(N-1 downto 0); -- MUX Output signal for ALUSRC that feeds to ALU port 2
  
  --Instruction Splices
  signal s_inst_32b ,s_inst_32b_exreg, s_inst_32b_zero , s_inst_32b_sign ,s_rd1_ex ,s_rd2_ex , s_rd2_mem, s_rd1_de, s_rd2_de  : std_logic_vector(DATA_WIDTH -1 downto 0); -- sign extended form of 15-0 bits of inst.
  signal s_25_21,s_25_21_ex, s_20_16,s_20_16_ex,s_bufferRegDst, s_reg_dest_mem, s_reg_dest_wb : std_logic_vector(4 downto 0);
  signal s_25_0: std_logic_vector(25 downto 0);
  signal s_31_26, s_5_0: std_logic_vector(5 downto 0);

  --PC count passthroughs
  signal s_PCPlus4_passthrough, s_currentAddr,s_PCPlus4_passthrough_ex,s_PCPlus4_passthrough_mem : std_logic_vector(DATA_WIDTH - 1 downto 0); -- passed through PCPlus4 for state reg
  --Control signals
  signal s_ALUOP,s_shamt,s_rd_id_ex_out        : std_logic_vector(4 downto 0); -- control signal
  signal s_MemToReg,s_regWR_CTL_IFReg     : std_logic;
  signal s_nextInst,s_notJumpAddress, s_memtoregbuff,main_plusFour,mux2fetch     : std_logic_vector(31 downto 0);
  
  signal s_ALURES, s_ALURES_mem, s_dMemtoMux, s_AddressToMux       : std_logic_vector(DATA_WIDTH - 1 downto 0); 

SIGNAL s_wb_control_temp : STD_LOGIC_VECTOR(3 downto 0);
  --control passthroughs
  signal s_control_id_ex, s_control_ex, s_control_wb: std_logic_vector(14 downto 0);

  signal s_control_mem, s_control_mem_in : std_logic_vector(8 downto 0); -- TODO

  signal s_wb_memtoReg : std_logic_vector(3 downto 0); -- temp control signal

  signal s_id_ex_control_15b: std_logic_vector(14 downto 0); 

  signal s_regdst_ex  : std_logic;

  SIGNAL s_shitty_immediate_value              : STD_LOGIC_VECTOR(31 DOWNTO 0);


  --jump control
  signal s_jr, s_jal : std_logic;

  signal s_jump_target, s_branch_target, s_vhdlSucks, s_PCSrc_next : std_logic_vector(DATA_WIDTH - 1 downto 0);--self explanatory, except VHDL sucks is the legacy final target signal.

  signal  s_DMemWr_buffer: std_logic;

  SIGNAL s_pcWrite, s_we_pcreg, s_pc_src_reg : STD_LOGIC;
begin

 
--implement TODO PC + 4 to $31, when JAL

-- on JR set next instruction to $31
--mux set nextInstr to JR that loads $31 when opcode == JR
--OvFL detection

  -- TODO: This is required to be your final input to your instruction memory. This provides a feasible method to externally load the memory module which means that the synthesis tool must assume it knows nothing about the values stored in the instruction memory. If this is not included, much, if not all of the design is optimized out because the synthesis tool will believe the memory to be all zeros.
  with iInstLd select
    s_IMemAddr <= s_NextInstAddr when '0',
      iInstAddr when others;

  

  IMem: mem
    generic map(ADDR_WIDTH => ADDR_WIDTH,
                DATA_WIDTH => N)
    port map(clk          => iCLK,
             addr         => s_IMemAddr(11 downto 2),
             data         => iInstExt,
             we           => iInstLd,
             q            => s_Inst);
  


--  oALUOut <= s_oALUOut; -- connect signal to output port
  --s_DMemData <= s_RegReadData2;
  oALUOut <= s_DMemAddr;
  s_25_21 <= s_Inst_de(25 downto 21);
  s_20_16 <= s_Inst_de(20 downto 16);
  s_25_0 <= s_Inst_de(25 downto 0);
  s_31_26 <= s_Inst_de(31 downto 26);

  s_5_0 <= s_Inst_de(5 downto 0);
  -- Instruction Memory
  --s_control_id_ex <= s_Inst_de(11 downto 0)


  pcreg: pc_reg 

		port map(i_CLK        => iCLK,     -- Clock input
			 i_RST             => iRST,-- Reset input
			 i_WE         => '1',     -- Write enable input
			 i_D          => s_nextInst,     -- Data value input
			 o_Q          => s_NextInstAddr);   -- Data value output


  fetch_hack: fetch
   port map(
        iCLK => iCLK,
        iRST => iRST,
        i_A  => s_IMemAddr,
        o_A  => s_PCSrc_next,--PC plus 4
        i_B  => s_inst_32b_exreg(29 downto 0),
        o_B  => s_temp
   ); 

  s_pc_src_reg <= '1' WHEN  (s_control_id_ex(7) = '1') ELSE -- MUX to correct for branch not taken.
    '1' WHEN s_control_id_ex(8) = '1' AND s_vhdlSucks = s_branch_target ELSE
      '0';

   

      --  s_PCSrc_next <= s_IMemAddr + X"00000004";

       mux_PCSrc: mux2t1_N
       generic map(N => 32)
         port map(
           i_S                   => s_pc_src_reg, --Whoops, this should be MemWrite not RegWr        
           i_D0                  => s_PCSrc_next,
           i_D1                  => s_vhdlSucks,
           o_O                   => s_nextInst
         );

  if_id: if_id_reg
  generic map(N => 96)
    port map(
        iCLK        => iCLK, -- Clock
        iRST        => iRST, -- Reset
        iWE         => '1', -- Write Enable
        iFlush      => s_flush,
        currentPC   => s_IMemAddr, -- Holds Current PC Address
        pcPlus4     => s_nextInst, -- Holds PC+4 Address
        instruction => s_Inst(31 downto 0), -- Holds Instruction
        oCurrentPC  => s_currentAddr, -- Outputs Current PC Address
        oPCPlus4    => s_PCPlus4_passthrough, -- Outputs PC+4 Address
        oInstruction=> s_Inst_de(31 downto 0) -- Outputs Instruction
     ); -- Outputs Flush



     

     ctrl_unit: controlunit
     port map(
     opcode                => s_31_26,
     funct                 => s_5_0,
     regDst                => s_regDst,-- To MUX control
     branch                => s_branch,-- To AND gate
     jump	                => s_jump, -- To jump MUX
     jr                    => s_jr,
     jal                   => s_jal,
     memRead               => open, -- To DMem
     memToReg              => s_MemToReg,-- to DMem output MUX
     ALUOp                 => s_ALUOP,-- to ALU
     memWrite              => s_DMemWr_buffer,-- to Dmem
     ALUSrc                => s_alusrc,-- to Mux before ALU
     regWrite              => s_regWr_temp,-- to Regfile
     extender              => s_extender,
     halt                  => s_halt_temp-- AS required. All Instructions Complete or Machine cessation.
   );
 
 


     

     jalAddr: mux2t1_N
     generic map(N => 5) 
     port map(i_S   => s_jal,
           i_D0      => s_20_16, --rt is taking the place of rd,
           i_D1      => "11111", -- 31
           o_O       => s_bufferRegDst);
   
   
       RegFile: register_file 
       port map(
         clk               => iCLK,
         rst               => iRST,
         WEn                => s_RegWr, -- to be passed from WB
         Read1          => s_25_21,
         Read2          => s_20_16,
         WriteR           => s_reg_dest_wb,
         data           => s_RegWrData(N-1 downto 0),
         oRead1          => s_rd1_de,
         oRead2          => s_rd2_de
     );

       s_RegReadData1 <= s_rd1_de;
     
     s_id_ex_control_15b(14 downto 10) <= s_ALUOP;
     s_id_ex_control_15b(9) <= s_ALUSrc;
     s_id_ex_control_15b(8) <= s_branch;
     s_id_ex_control_15b(7) <= s_jump;
     s_id_ex_control_15b(6) <= s_jr;
     s_id_ex_control_15b(5) <= s_jal;
     s_id_ex_control_15b(4) <= s_regWr_temp;
     s_id_ex_control_15b(3) <= s_MemToReg;
     s_id_ex_control_15b(2) <= s_DMemWr_buffer;
     s_id_ex_control_15b(1) <= s_alusrc;
     s_id_ex_control_15b(0) <= s_halt_temp;

     m_sign_ex: sign_extender 
         generic map(INPUT_WIDTH => 16)
         port map (
         inputs              => s_Inst_de(15 downto 0),
         outputs             => s_inst_32b_sign(DATA_WIDTH - 1 downto 0)
       );
   
       m_zero_ex: zero_extender
       generic map(INPUT_WIDTH => 16)
         port Map(
         inputs => s_Inst_de(15 downto 0),
         outputs => s_inst_32b_zero(DATA_WIDTH - 1 downto 0)
   
       );
   
       extender_mux: mux2t1_N
        generic map(N => 32) 
        port map(i_S   => s_extender, -- 0 for sign, 1 for zero
           i_D0      => s_inst_32b_sign, --rt is taking the place of rd,
           i_D1      => s_inst_32b_zero, -- 31
           o_O       => s_inst_32b
           
           );


  id_ex: id_ex_reg
     generic map(N => 164)
     port map(
        iCLK        => iCLK, -- Clock
        iRST        => iRST, -- Reset
        iWE         => '1', -- Write Enable
        regdst      => s_regDst,
        iFlush      => s_flush,
         shamt       => s_Inst_de(10 downto 6),    
         control     => s_id_ex_control_15b, -- Control Signals
         pcPlus4     => s_PCPlus4_passthrough, -- PC+4
         readData1   => s_rd1_de, -- Read Data 1 from Register File
         readData2   => s_rd2_de, -- Read Data 2 from Register File
         signExtend  => s_inst_32b, -- Sign Extended Immediate
         rsAddress   => s_25_21, -- Register Address 1
         rtAddress   => s_bufferRegDst, -- Register Address 2
         rdAddress   => s_inst_de(15 downto 11), -- Register Address 3
         oregdst     => s_regdst_ex,
         oControl    => s_control_id_ex, 
         oShamt      => s_shamt,
         oPcPlus4    => s_PCPlus4_passthrough_ex, -- TODO Make more signals``
         oReadData1  => s_rd1_ex,
         oReadData2  => s_rd2_ex,
         oSignExtend => s_inst_32b_exreg,
         oRsAddress  => s_25_21_ex,
         oRtAddress  => s_20_16_ex,
         oRdAddress  => s_rd_add_ex
      );
    

      mux_reg_dst: mux2t1_N -- MOVE TO EX STAGE  
      generic map(N => 5)
        port map(
        i_S                   => s_regdst_ex,         
        i_D1                  => s_rd_add_ex,
        i_D0                  => s_20_16_ex, -- either normal or address 31 for JAL
        o_O                   => s_rd_id_ex_out
      );
   
    s_flush <= '1' WHEN s_control_id_ex(7) = '1' OR s_control_id_ex(8) = '1' ELSE '0';



      mux_alusrc: mux2t1_N
      generic map(N => 32)
        port map(
        i_S                   => s_control_id_ex(9),         
        i_D0                  => s_rd2_ex(DATA_WIDTH - 1 downto 0),
        i_D1                  => s_inst_32b_exreg(DATA_WIDTH - 1 downto 0),
        o_O                   => s_ALU2
      );



           alu_inst: ALU
           port map(
           i_A  		              => s_rd1_ex,    	 	--Read data input from register file
           i_B    		            => s_ALU2,    	 		--Read data input after the mux from register file data 2 and the sign extension
           i_ALUOP    	     	    => s_control_id_ex(14 downto 10),    	 		--Input from control unit
           i_shamt    	     	    => s_shamt,    	--control output passthrough from id/ex
           o_F    		            => s_ALURES,   		--output of the ALU that goes to data memory and the mux of the output of data memory
           o_Overflow    	      => s_ovfl_ex,  			--overflow output
           o_Zero    	     	    => s_zeroToFetch    		--zero output that goes to jump calc 
         );



       
         jalDat: mux2t1_N
         generic map(N => 32) 
         port map(i_S   => s_jal,
               i_D0      => s_ALURES, --ALU output
               i_D1      => s_PCPlus4_passthrough_ex,
               o_O       => s_memtoregbuff);
          


  s_jump_target <= s_PCPlus4_passthrough(31 DOWNTO 28) & (s_inst_32b_exreg(25 DOWNTO 0) & "00");
  s_shitty_immediate_value <= s_jump_target OR X"00400000";
  -- s_temp <= s_inst_32b(29 downto 0) & "00";

  -- 3 branch
  -- 4 jump
  -- 5 jr
  -- 6 jal
  add1 : full_adder_N
  PORT MAP(
    i_Xv => s_PCPlus4_passthrough_ex,
    i_Yv => s_temp,
    i_Cv => '0',
    o_Sv => s_branch_target,
    o_Cv => OPEN
  );

--BEQ
  s_vhdlSucks <= s_branch_target WHEN (s_control_id_ex(7) = '0' AND s_control_id_ex(8) = '1' AND s_zeroToFetch = '1' AND s_control_id_ex(14 DOWNTO 10) = "01010") ELSE
    --s_rd1_de WHEN (s_control_id_ex(6) = '1') ELSE -- JAL
    s_rd1_ex WHEN s_control_id_ex(6) = '1' ELSE -- changed back by one cycle
    s_branch_target WHEN (s_control_id_ex(7) = '0' AND s_control_id_ex(8) = '1' AND s_zeroToFetch = '0' AND s_control_id_ex(14 DOWNTO 10) = "01001") ELSE -- BNE
    s_shitty_immediate_value WHEN (s_control_id_ex(7) = '1') AND NOT s_control_id_ex(6) = '1' ELSE -- Jump
    s_PCPlus4_passthrough_ex;

    
  --END FETCH CODE --

  s_control_mem_in(8 DOWNTO 5) <= "0000"; -- placeholder bits
  s_control_mem_in(0) <= s_control_id_ex(4); -- RegWr
  s_control_mem_in(1) <= s_control_id_ex(3); -- Mem2Reg
  s_control_mem_in(2) <= s_control_id_ex(0); -- Halt
  s_control_mem_in(3) <= s_control_id_ex(2); -- MemWrite
  s_control_mem_in(4) <= s_Ovfl_ex; -- overflow bit


    ex_mem: ex_mem_reg
      generic map(N => 110)
      port map(
        iCLK        => iCLK,
        iRST        => iRST,
        iWE         => '1',
        control     => s_control_mem_in,--s_control_mem_in, --TODO needs certain bits
        pcPlus4     => s_PCPlus4_passthrough_ex,--Needs fetch/jump handling logic,
        regDst      => s_rd_id_ex_out,-- Stores Destination Register Address
        ALURes      => s_memtoregbuff,
        readData2   => s_rd2_ex,
        oControl    => s_control_mem(8 downto 0),
        oPCPlus4    => s_PCPlus4_passthrough_mem,
        oRegDst     => s_reg_dest_mem,
        oALURes     => s_DMemAddr, -- Goes to DMem
        oReadData2  => s_DMemData
      );




        -- Data Memory
  DMem: mem
  generic map(ADDR_WIDTH => ADDR_WIDTH,
              DATA_WIDTH => N)
  port map(clk          => iCLK,
           addr         => s_DMemAddr(11 downto 2),
           data         => s_DMemData,
           we           => s_control_mem(3),
           q            => s_DMemOut);

    s_DMemWr <= s_control_mem(3);
    

    s_wb_control_temp(2 downto 0) <= s_control_mem(2 downto 0);
    s_wb_control_temp(3) <= s_control_mem(4); -- ovfl


    mem_web: mem_wb_reg -- TODO
      generic map(N => 73)
      port map(
      iCLK       => iCLK,
      iRST       => iRST,
      iWE        => '1',
      control    => s_wb_control_temp, -- What signals are needed?
      regDst     => s_reg_dest_mem, -- Stores Destination Register Address
      memRead    => s_DMemOut, -- Stores Data Read from Memory
      ALURes     => s_dMemAddr, -- Stores Result of ALU
      oControl   => s_wb_memtoReg, -- See input above
      oRegDst    => s_reg_dest_wb,
      oMemRead   => s_dMemtoMux,
      oALURes    => s_AddressToMux
      );
        s_Ovfl <= s_wb_memtoReg(3); -- Overflow handling. Hopefully.


      mux_memToReg: mux2t1_N
    generic map(N => 32)
    port map(
    i_S                   => s_wb_memtoReg(1),         --bit 0, regWr, bit 1, MemToReg
    i_D0                  => s_AddressToMux,
    i_D1                  => s_dMemtoMux,
    o_O                   => s_RegWrData(DATA_WIDTH -1 downto 0)
);


  s_Halt <= s_wb_memtoReg(2); -- Halt Signal in Writeback

  s_RegWrAddr <= s_reg_dest_wb; -- Write Address written back

  s_RegWr <= s_wb_memtoReg(0); -- RegWRite Signal in WB









































	--s_DMemData <= s_oALUOut;

  -- jReg: mux2t1_N -- mux for JR
  -- generic map(N => 32) 
  -- port map(i_S    => s_jr,
  --       i_D0      => s_notJumpAddress,
  --       i_D1      => s_RegReadData1,
  --       o_O       => s_NextInstAddr);

-- jReg2: mux2t1_N
-- generic map(N => 32) 
-- port map(i_S    => s_jr,
--       i_D0      => s_IMemAddr,
--       i_D1      => s_RegReadData1,
--       o_O       => mux2fetch);



   



  -- Fetch_Logic: fetch
  --   generic map(N => 32)
	--   port map(
  -- 	i_CLK    	            => iCLK,
  -- 	i_RST    	            => iRST,
  -- 	i_A    		            => s_IMemAddr, 		--pc address input, example:10010000
  -- 	i_signExt 	          => s_inst_32b,   	--sign extended 32 bit number after the sign extension from 16 to 32
  -- 	i_mem    	            => s_25_0, --gives 26 bit value from instructin memory
  -- 	i_zero    	          => s_zeroToFetch,    	--zero input from ALU
  -- 	i_jump    	          => s_jump,   	 	--jump input from control unit
  -- 	i_branch   	          => s_branch,   	 	--branch input value from control unit
  -- 	i_enable    	        => '1',  	 	--enable from register(PC) or not - (we can probably get rid of this and just input 1 into the enable for the PC register within fetch)
  --   i_ALUOP               => s_ALUOP,
  --   i_jr                  => s_jr,
  --   i_jrdat               => s_RegReadData1,
  --   o_plusFour            => main_plusFour,
  -- 	o_A    		            => s_nextInst   	--output of the last mux that then inputs back into the PC register
	-- );
 



 


 -- reg_fetch_decode: mux32t1



  --Jump: mux2t1_N
  --generic map(N => 32) 
  --  port map(i_S    => s_jump,
  --    i_D0      => s_normalOrBranch, 
  --    i_D1      => s_finalJumpAddress,
  --    o_O       => s_inputPC);

  -- TODO: Ensure that s_Halt is connected to an output control signal produced from decoding the Halt instruction (Opcode: 01 0100)
  -- TODO: Ensure that s_Ovfl is connected to the overflow output of your ALU
  -- TODO: Implement the rest of your processor below this comment! 
end structure;

