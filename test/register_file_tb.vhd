-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- register_file_tb.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains an testbench of a register file.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity register_file_tb is
end register_file_tb;

architecture testbench of register_file_tb is

    signal clk          : std_logic := '0';
    signal rst          : std_logic := '0';
    signal WEn          : std_logic := '0';
    signal WriteR       : std_logic_vector(4 downto 0) := "00000";
    signal Read1        : std_logic_vector(4 downto 0) := "00000";
    signal Read2        : std_logic_vector(4 downto 0) := "00000";
    signal data         : std_logic_vector(31 downto 0) := (others => '0');
    signal oRead1       : std_logic_vector(31 downto 0);
    signal oRead2       : std_logic_vector(31 downto 0);

    constant clk_period : time := 10 ns;  -- Adjust as needed

    component register_file
        port(clk        : in std_logic; 
             rst        : in std_logic; -- Reset
             WEn        : in std_logic; -- Write Enable
             WriteR     : in std_logic_vector(4 downto 0); -- Write Select
             Read1      : in std_logic_vector(4 downto 0); -- Read 1 Select
             Read2      : in std_logic_vector(4 downto 0); -- Read 2 Select
             data       : in std_logic_vector(31 downto 0); -- Write Data
             oRead1     : out std_logic_vector(31 downto 0); -- Data from Register 1
             oRead2     : out std_logic_vector(31 downto 0)); -- Data from Register 2
        
    end component;

begin

    uut: register_file
        port map(clk        => clk,
                 rst        => rst,
                 WEn        => WEn,
                 WriteR     => WriteR,
                 Read1      => Read1,
                 Read2      => Read2,
                 data       => data,
                 oRead1     => oRead1,
                 oRead2     => oRead2);

    -- Clock generation process
    process
    begin
        while now < 5000 ns loop  -- Simulate for 50 us
            clk <= '0';
            wait for clk_period / 2;
            clk <= '1';
            wait for clk_period / 2;
        end loop;
        wait;
    end process;

    -- Stimulus generation process
    process
    begin
        rst <= '1';
        wait for 20 ns;
        rst <= '0';
        wait for 20 ns;

        WEn <= '1';
        data <= "00000000000000001111111111111111";
        WriteR <= "00001";
        Read1 <= "00001";
        Read2 <= "00000";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '1';
        data <= "10101010101010101010101010101010";
        WriteR <= "00010";
        Read1 <= "00001";
        Read2 <= "00010";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '0';
        data <= "00000000000000000000000000000000";
        WriteR <= "00001";
        Read1 <= "00001";
        Read2 <= "00010";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '1';
        data <= "11111111111111111111111111111111";
        WriteR <= "00000";
        Read1 <= "00000";
        Read2 <= "00010";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '1';
        data <= "11111111111111111111111111111111";
        WriteR <= "11011";
        Read1 <= "11011";
        Read2 <= "00010";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '1';
        data <= "00110011001100110011001100110011";
        WriteR <= "01010";
        Read1 <= "01010";
        Read2 <= "11011";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '0';
        data <= "11111111111111111111111111111111";
        WriteR <= "01010";
        Read1 <= "01010";
        Read2 <= "00000";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '1';
        data <= "00000000000000001111111111111111";
        WriteR <= "11111";
        Read1 <= "11111";
        Read2 <= "01010";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '1';
        data <= "11111111111111110000000000000000";
        WriteR <= "00001";
        Read1 <= "00001";
        Read2 <= "11111";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '0';
        data <= "00000000000000001111111111111111";
        WriteR <= "00001";
        Read1 <= "00001";
        Read2 <= "11111";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '1';
        data <= "00000000000000001111111111111111";
        WriteR <= "11011";
        Read1 <= "11011";
        Read2 <= "00001";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '0';
        data <= "00001111100000001111111111111111";
        WriteR <= "11011";
        Read1 <= "11011";
        Read2 <= "00001";
        wait for 20 ns;
        WEn <= '0';

        WEn <= '0';
        data <= "00001111100000001111111111111111";
        WriteR <= "00001";
        Read1 <= "00001";
        Read2 <= "11011";
        wait for 20 ns;
        WEn <= '0';

        wait;
    end process;

end testbench;
