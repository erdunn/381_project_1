library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux_32x1_tb is
end mux_32x1_tb;

architecture Behavioral of mux_32x1_tb is
    signal Inputs : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
    signal Selects : STD_LOGIC_VECTOR (4 downto 0) := (others => '0');
    signal Output : STD_LOGIC;
begin
    uut: entity work.mux_32x1
        port map (
            Inputs => Inputs,
            Selects => Selects,
            Output => Output
        );

    stimulus: process
    begin
        -- Edge Case 1: All inputs are '0', Selects = "00000"
        Inputs <= (others => '0');
        Selects <= "00000";
        wait for 10 ns;

        -- Edge Case 2: All inputs are '1', Selects = "11111"
        Inputs <= (others => '1');
        Selects <= "11111";
        wait for 10 ns;

        -- Edge Case 3: Alternating '1' and '0' in inputs, Selects = "10101"
        Inputs <= "10101010101010101010101010101010";
        Selects <= "10101";
        wait for 10 ns;

        -- Edge Case 4: Alternating '0' and '1' in inputs, Selects = "01010"
        Inputs <= "0101010101010101010";
	end process;
end;