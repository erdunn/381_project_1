library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;

entity tb_ALU is
  generic(gCLK_HPER   : time := 10 ns);   -- Generic for half of the clock cycle period
end tb_ALU;

architecture behavior of tb_ALU is

constant cCLK_PER  : time := gCLK_HPER * 2;

component ALU is
generic(N : integer := 32);
	port(i_A	: in std_logic_vector(N-1 downto 0);
	     i_B	: in std_logic_vector(N-1 downto 0);
	     i_ALUOP	: in std_logic_vector(3 downto 0);
	     i_shamt	: in std_logic_vector(4 downto 0);
	     o_F	: out std_logic_vector(N-1 downto 0);
	     --o_CarryOut	: out std_logic;
	     o_Overflow	: out std_logic;
	     o_Zero	: out std_logic);
end component;

signal s_CLK, s_RST      : std_logic := '0';

signal s_A 		: std_logic_vector(31 downto 0);
signal s_B		: std_logic_vector(31 downto 0);
signal s_ALUOP		: std_logic_vector(3 downto 0);
signal s_shamt		: std_logic_vector(4 downto 0);
signal s_F 		: std_logic_vector(31 downto 0);
signal s_Overflow	: std_logic;
signal s_Zero		: std_logic;

begin

DUT0: ALU
  port map(i_A		=> s_A,
	   i_B		=> s_B,
	   i_ALUOP	=> s_ALUOP,
	   i_shamt	=> s_shamt,
	   o_F		=> s_F,
	   o_Overflow	=> s_Overflow,
	   o_Zero	=> s_Zero);

P_CLK: process
 	 begin
   	 s_CLK <= '0';
    	wait for gCLK_HPER;
    	s_CLK <= '1';
   	 wait for gCLK_HPER;
  	end process;

	P_TB: process
	begin

	s_A <= x"00010101"; --testing and
	s_B <= x"00001010";
	s_ALUOP <= "0000";
    wait for cCLK_PER;

	s_A <= x"00010101"; --testing or
	s_B <= x"00001010";
	s_ALUOP <= "0001";
    wait for cCLK_PER;

	s_A <= x"00010101"; --testing xor
	s_B <= x"00001010";
	s_ALUOP <= "0010";
    wait for cCLK_PER;

	s_A <= x"00010101"; --testing nor
	s_B <= x"00001010";
	s_ALUOP <= "0011";
    wait for cCLK_PER;

	s_A <= x"00010101"; --testing add
	s_B <= x"00001010";
	s_ALUOP <= "0110";
    wait for cCLK_PER;

	s_A <= x"00010101"; --testing sub
	s_B <= x"00001010";
	s_ALUOP <= "1110";
    wait for cCLK_PER;

	s_shamt <= "00100";
	s_A <= x"00010101"; --testing sll
	s_B <= x"00001010";
	s_ALUOP <= "0100";
    wait for cCLK_PER;

	s_shamt <= "01000";
	s_A <= x"00010101"; --testing srl
	s_B <= x"00001010";
	s_ALUOP <= "0101";
    wait for cCLK_PER;

	s_shamt <= "00010";
	s_A <= x"00010101"; --testing sra
	s_B <= x"00001011";
	s_ALUOP <= "0111";
    wait for cCLK_PER;

	s_A <= x"00010101"; --testing slt
	s_B <= x"00001010";
	s_ALUOP <= "1111";
    wait for cCLK_PER;

	s_A <= x"00000000"; --testing zero
	s_B <= x"00000000";
	s_ALUOP <= "0110";
    wait for cCLK_PER;

    wait;
    end process;

end behavior;