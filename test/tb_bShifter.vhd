library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_bShifter is
end tb_bShifter;

architecture Behavioral of tb_bShifter is
    signal i_X : STD_LOGIC_VECTOR(7 downto 0);
    signal i_S0, i_S1 : STD_LOGIC;
    signal o_Y : STD_LOGIC_VECTOR(7 downto 0);
    
    component bShifter is
        generic ( N : integer := 8);
        Port (
            i_X   : in STD_LOGIC_VECTOR(N-1 downto 0);
            i_S0  : in STD_LOGIC;
            i_S1  : in STD_LOGIC;
            o_Y   : out STD_LOGIC_VECTOR(N-1 downto 0)
        );
    end component;
    
begin
    UUT: bShifter generic map (N => 8) port map (i_X, i_S0, i_S1, o_Y);

    process
    begin
        -- Test case 1: Logical left shift by 1
        i_X <= "11011010";
        i_S0 <= '0';
        i_S1 <= '0';
        wait for 15 ns;
        assert o_Y = "10110100" report "Error: Logical left shift by 1" severity ERROR;

        -- Test case 2: Logical right shift by 1
        i_X <= "11011010";
        i_S0 <= '1';
        i_S1 <= '0';
        wait for 15 ns;
        assert o_Y = "01101101" report "Error: Logical right shift by 1" severity ERROR;

        -- Test case 3: Left rotate by 1
        i_X <= "11011010";
        i_S0 <= '0';
        i_S1 <= '1';
        wait for 15 ns;
        assert o_Y = "01101101" report "Error: Left rotate by 1" severity ERROR;

        -- Test case 4: Right rotate by 1
        i_X <= "11011010";
        i_S0 <= '1';
        i_S1 <= '1';
        wait for 15 ns;
        assert o_Y = "01101101" report "Error: Right rotate by 1" severity ERROR;
        
        -- Additional test cases can be added as needed
        
        -- Stop simulation
        wait;
    end process;
end Behavioral;
