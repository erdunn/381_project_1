-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- tb_controlunit.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains the implementation for a testbench of a MIPS Control Unit.
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity tb_controlunit is
end tb_controlunit;

architecture tb_architecture of tb_controlunit is
    -- Define signals for the testbench
    signal opcode           : std_logic_vector(5 downto 0) := (others => '0');
    signal funct            : std_logic_vector(5 downto 0) := (others => '0');
    signal regDst           : std_logic := '0';
    signal branch           : std_logic := '0';
    signal jump             : std_logic := '0';
    signal memRead          : std_logic := '0';
    signal memToReg         : std_logic := '0';
    signal ALUOp            : std_logic_vector(3 downto 0) := "0000";
    signal memWrite         : std_logic := '0';
    signal ALUSrc           : std_logic := '0';
    signal regWrite         : std_logic := '0';
    signal halt             : std_logic := '0';
begin
    -- Instantiate the control unit
    UUT: entity work.controlunit
    port map (
        opcode          => opcode,
        funct           => funct,
        regDst          => regDst,
        branch          => branch,
        jump            => jump,
        memRead         => memRead,
        memToReg        => memToReg,
        ALUOp           => ALUOp,
        memWrite        => memWrite,
        ALUSrc          => ALUSrc,
        regWrite        => regWrite,
        halt            => halt
    );

    -- Stimulus process
    process
    begin
        -- Test vector 1
        opcode      <= "000000";
        funct       <= "100000"; -- ADD
        wait for 10 ns;
        
        -- Test vector 2
        opcode      <= "001000";
        funct       <= (others => '0'); -- ADDI 
        wait for 10 ns;

        -- Test vector 3
        opcode      <= "100011";
        funct       <= "000000"; -- LW
        wait for 10 ns;

        -- Test vector 4
        opcode      <= "001110";
        funct       <= "000000"; -- XORI
        wait for 10 ns;

        --Test vector 5
        opcode      <= "000000";
        funct       <= "100100"; -- AND
        wait for 10 ns;

        --Test vector 6
        opcode      <= "000000";
        funct       <= "100101"; -- OR
        wait for 10 ns;

        --Test vector 7
        opcode      <= "000000";
        funct       <= "100010"; -- SUB
        wait for 10 ns;

        --Test vector 8
        opcode      <= "000000";
        funct       <= "100110"; -- NOR
        wait for 10 ns;

        --Test vector 9
        opcode      <= "000000";
        funct       <= "101010"; -- SLT
        wait for 10 ns;

        --Test vector 10
        opcode      <= "000000";
        funct       <= "000000"; -- SLL
        wait for 10 ns;

        --Test vector 11
        opcode      <= "010100";
        funct       <= "000000"; -- Halt
        wait for 10 ns;
        wait;
    end process;
end tb_architecture;
