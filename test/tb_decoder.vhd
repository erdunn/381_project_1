library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decoder_tb is
end decoder_tb;

architecture Behavioral of decoder_tb is
    signal I : STD_LOGIC_VECTOR (4 downto 0);
    signal O : STD_LOGIC_VECTOR (31 downto 0);
begin
    uut: entity work.decoder_5x32
        port map (
            I => I,
            O => O
        );

    stimulus: process
    begin
        -- Test Case 1: I = "00000"
        I <= "00000";
        wait for 10 ns;

        -- Test Case 2: I = "00001"
        I <= "00001";
        wait for 10 ns;

        -- Test Case 3: I = "00010"
        I <= "00010";
        wait for 10 ns;

        -- Test Case 4: I = "00100"
        I <= "00100";
        wait for 10 ns;

        -- Test Case 5: I = "01000"
        I <= "01000";
        wait for 10 ns;

        -- Test Case 6: I = "10000"
        I <= "10000";
        wait for 10 ns;

        -- Test Case 7: I = "11000"
        I <= "11000";
        wait for 10 ns;

        wait;
    end process;
end Behavioral;
