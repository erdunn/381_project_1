library IEEE;
use IEEE.std_logic_1164.all;

entity tb_register is
  generic (
    gCLK_HPER   : time := 50 ns
  );
end tb_register;

architecture behavior of tb_register is
  -- Calculate the clock period as twice the half-period
  constant cCLK_PER  : time := gCLK_HPER * 2;

  component NBitRegister
    generic (
      N : integer := 32
    );
    port (
      i_CLK      : in  std_logic;
      i_RST      : in  std_logic;
      i_WE       : in  std_logic;
      i_D        : in  std_logic_vector(N-1 downto 0);
      o_Q        : out std_logic_vector(N-1 downto 0)
    );
  end component;

  -- Signals for the N-bit register
  signal s_CLK, s_RST, s_WE  : std_logic;
  signal s_D, s_Q : std_logic_vector(31 downto 0);

begin
  -- Instantiate the N-bit register
  DUT: NBitRegister
  generic map (
    N => 32
  )
  port map (
    i_CLK => s_CLK,
    i_RST => s_RST,
    i_WE  => s_WE,
    i_D(0)   => s_D(0),
    i_D(1)   => s_D(1),
    i_D(2)   => s_D(2),
    i_D(3)   => s_D(3),
    i_D(4)   => s_D(4),
    i_D(5)   => s_D(5),
    i_D(6)   => s_D(6),
    i_D(7)   => s_D(7),
    i_D(8)   => s_D(8),
    i_D(9)   => s_D(9),
    i_D(10)  => s_D(10),
    i_D(11)  => s_D(11),
    i_D(12)  => s_D(12),
    i_D(13)  => s_D(13),
    i_D(14)  => s_D(14),
    i_D(15)  => s_D(15),
    i_D(16)  => s_D(16),
    i_D(17)  => s_D(17),
    i_D(18)  => s_D(18),
    i_D(19)  => s_D(19),
    i_D(20)  => s_D(20),
    i_D(21)  => s_D(21),
    i_D(22)  => s_D(22),
    i_D(23)  => s_D(23),
    i_D(24)  => s_D(24),
    i_D(25)  => s_D(25),
    i_D(26)  => s_D(26),
    i_D(27)  => s_D(27),
    i_D(28)  => s_D(28),
    i_D(29)  => s_D(29),
    i_D(30)  => s_D(30),
    i_D(31)  => s_D(31),
    o_Q   => s_Q
  );

  -- Clock generation process
  P_CLK: process
  begin
    s_CLK <= '0';
    wait for gCLK_HPER;
    s_CLK <= '1';
    wait for gCLK_HPER;
  end process;

  -- Testbench process
  P_TB: process
  begin
    -- Reset the register
    s_RST <= '1';
    s_WE  <= '0';
    s_D   <= (others => '0');
    wait for cCLK_PER;

    -- Store '1' in the register
    s_RST <= '0';
    s_WE  <= '1';
    s_D(0)   <= '1';  -- Store 32'b00000000000000000000000000000001
    wait for cCLK_PER;

    -- Keep '1' in the register
    s_RST <= '0';
    s_WE  <= '0';
    wait for cCLK_PER;

    -- Store '0' in the register
    s_WE  <= '1';
    s_D(0)   <= '0';  -- Store 32'b00000000000000000000000000000000
    wait for cCLK_PER;

    -- Keep '0' in the register
    s_WE  <= '0';
    wait for cCLK_PER;

    wait;
  end process;

end behavior;
