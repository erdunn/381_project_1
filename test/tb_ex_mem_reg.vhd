-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- tb_ex_mem_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbench for the EX/MEM Register.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_ex_mem_reg is
end tb_ex_mem_reg;

architecture testbench of tb_ex_mem_reg is
    -- Constants and signals declaration
    constant N : integer := 110;
    
    signal iCLK        : std_logic := '0';
    signal iRST        : std_logic := '0';
    signal iWE         : std_logic := '0';
    signal control     : std_logic_vector(8 downto 0) := (others => '0');
    signal pcPlus4     : std_logic_vector(31 downto 0) := (others => '0');
    signal regDst      : std_logic_vector(4 downto 0) := (others => '0');
    signal ALURes      : std_logic_vector(31 downto 0) := (others => '0');
    signal readData2   : std_logic_vector(31 downto 0) := (others => '0');
    signal oControl    : std_logic_vector(8 downto 0);
    signal oPCPlus4    : std_logic_vector(31 downto 0);
    signal oRegDst     : std_logic_vector(4 downto 0);
    signal oALURes     : std_logic_vector(31 downto 0);
    signal oReadData2  : std_logic_vector(31 downto 0);

    -- Component instantiation
    component ex_mem_reg
        generic(N : integer := 110);
        port(
            iCLK        : in std_logic;
            iRST        : in std_logic;
            iWE         : in std_logic;
            control     : in std_logic_vector(8 downto 0);
            pcPlus4     : in std_logic_vector(31 downto 0);
            regDst      : in std_logic_vector(4 downto 0);
            ALURes      : in std_logic_vector(31 downto 0);
            readData2   : in std_logic_vector(31 downto 0);
            oControl    : out std_logic_vector(8 downto 0);
            oPCPlus4    : out std_logic_vector(31 downto 0);
            oRegDst     : out std_logic_vector(4 downto 0);
            oALURes     : out std_logic_vector(31 downto 0);
            oReadData2  : out std_logic_vector(31 downto 0)
        );
    end component;

begin
    -- DUT (Design Under Test) instantiation
    DUT: ex_mem_reg
        generic map(N => N)
        port map(
            iCLK        => iCLK,
            iRST        => iRST,
            iWE         => iWE,
            control     => control,
            pcPlus4     => pcPlus4,
            regDst      => regDst,
            ALURes      => ALURes,
            readData2   => readData2,
            oControl    => oControl,
            oPCPlus4    => oPCPlus4,
            oRegDst     => oRegDst,
            oALURes     => oALURes,
            oReadData2  => oReadData2
        );

    -- Clock process
    process
    begin
        while now < 100 ns loop
            iCLK <= not iCLK; -- Toggle clock every half period
            wait for 5 ns;     -- Wait for 5 ns for the next clock edge
        end loop;
        wait;
    end process;

    -- Stimulus process (you can modify this part to apply different test cases)
    process
    begin
        wait for 10 ns; -- Initial wait
        iWE <= '1';     -- Enable write
        wait for 10 ns;
        -- Apply input values for testing
        control <= "000000000";
        pcPlus4 <= "00000000000000000000000000001000";
        regDst <= "00101";
        ALURes <= "00000000000000000000000000101010";
        readData2 <= "00000000000000000000000000111111";
        wait for 10 ns;
        iWE <= '0'; -- Disable write
        wait;
    end process;

end testbench;
