-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- tb_fetch.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains the testbench for the fetch logic.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_fetch is
end tb_fetch;

architecture tb_architecture of tb_fetch is
    -- Signals for fetch module ports
    signal CLK      : std_logic := '0';
    signal RST      : std_logic := '0';
    signal Ins_in   : std_logic_vector(31 downto 0) := (others => '0');
    signal Ins31_26 : std_logic_vector(5 downto 0);
    signal Ins25_21 : std_logic_vector(4 downto 0);
    signal Ins20_16 : std_logic_vector(4 downto 0);
    signal Ins15_11 : std_logic_vector(4 downto 0);
    signal Ins10_6  : std_logic_vector(4 downto 0);
    signal Ins5_0   : std_logic_vector(5 downto 0);
    signal Ins15_0  : std_logic_vector(15 downto 0);
begin
    -- Instantiate the fetch module
    UUT: entity work.fetch 
    port map(
        CLK             => CLK,
        RST             => RST,
        Ins_in          => Ins_in,
        Ins31_26        => Ins31_26,
        Ins25_21        => Ins25_21,
        Ins20_16        => Ins20_16,
        Ins15_11        => Ins15_11,
        Ins10_6         => Ins10_6,
        Ins5_0          => Ins5_0,
        Ins15_0         => Ins15_0
    );
    -- Stimulus process
    process
    begin
        -- Test vector 1
        RST <= '1';
        wait for 10 ns;
        RST <= '0';

        -- Test vector 2
        Ins_in <= "00010010000000000000000000001100"; -- Example input instruction
        wait for 10 ns;
        
        -- Add more test vectors as needed

        wait;
    end process;
end tb_architecture;
