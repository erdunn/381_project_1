-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- controlunit.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains the testbench for the forwarding unit.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_forwarding_unit is
end tb_forwarding_unit;

architecture sim of tb_forwarding_unit is
  -- Component declaration for the forwarding_unit
  component forwarding_unit
    port(
      EX_MEM_RegRd     : in std_logic_vector(4 downto 0);
      ID_EX_RegRs      : in std_logic_vector(4 downto 0);
      ID_EX_RegRt      : in std_logic_vector(4 downto 0);
      MEM_WB_RegRd     : in std_logic_vector(4 downto 0);
      EX_MEM_RegWrite  : in std_logic;
      MEM_WB_RegWrite  : in std_logic;
      Forward_A        : out std_logic_vector(1 downto 0);
      Forward_B        : out std_logic_vector(1 downto 0)
    );
  end component;

  -- Signals for testbench
  signal EX_MEM_RegRd_tb    : std_logic_vector(4 downto 0) := "00000";
  signal ID_EX_RegRs_tb     : std_logic_vector(4 downto 0) := "00000";
  signal ID_EX_RegRt_tb     : std_logic_vector(4 downto 0) := "00000";
  signal MEM_WB_RegRd_tb    : std_logic_vector(4 downto 0) := "00000";
  signal EX_MEM_RegWrite_tb : std_logic := '0';
  signal MEM_WB_RegWrite_tb : std_logic := '0';
  signal Forward_A_tb       : std_logic_vector(1 downto 0);
  signal Forward_B_tb       : std_logic_vector(1 downto 0);

begin
  -- Instantiate the forwarding_unit
  UUT: forwarding_unit port map (
    EX_MEM_RegRd     => EX_MEM_RegRd_tb,
    ID_EX_RegRs      => ID_EX_RegRs_tb,
    ID_EX_RegRt      => ID_EX_RegRt_tb,
    MEM_WB_RegRd     => MEM_WB_RegRd_tb,
    EX_MEM_RegWrite  => EX_MEM_RegWrite_tb,
    MEM_WB_RegWrite  => MEM_WB_RegWrite_tb,
    Forward_A        => Forward_A_tb,
    Forward_B        => Forward_B_tb
  );

  -- Stimulus process
  stim_proc: process
  begin
    -- Test case 1
    EX_MEM_RegRd_tb <= "01010";
    ID_EX_RegRs_tb <= "01100";
    ID_EX_RegRt_tb <= "00101";
    MEM_WB_RegRd_tb <= "00000";
    EX_MEM_RegWrite_tb <= '0';
    MEM_WB_RegWrite_tb <= '0';
    wait for 10 ns;

    -- Test case 2
    EX_MEM_RegRd_tb <= "00000";
    ID_EX_RegRs_tb <= "00101";
    ID_EX_RegRt_tb <= "10010";
    MEM_WB_RegRd_tb <= "00101";
    EX_MEM_RegWrite_tb <= '1';
    MEM_WB_RegWrite_tb <= '1';
    wait for 10 ns;

    -- Test case 3
    EX_MEM_RegRd_tb <= "00000";
    ID_EX_RegRs_tb <= "00101";
    ID_EX_RegRt_tb <= "10010";
    MEM_WB_RegRd_tb <= "00101";
    EX_MEM_RegWrite_tb <= '0';
    MEM_WB_RegWrite_tb <= '0';
    wait for 10 ns;

    -- Test case 4
    EX_MEM_RegRd_tb <= "00100";
    ID_EX_RegRs_tb <= "00001";
    ID_EX_RegRt_tb <= "00001";
    MEM_WB_RegRd_tb <= "00101";
    EX_MEM_RegWrite_tb <= '1';
    MEM_WB_RegWrite_tb <= '1';
    wait for 10 ns;

    -- Test case 5
    EX_MEM_RegRd_tb <= "00000";
    ID_EX_RegRs_tb <= "00101";
    ID_EX_RegRt_tb <= "10010";
    MEM_WB_RegRd_tb <= "00101";
    EX_MEM_RegWrite_tb <= '1';
    MEM_WB_RegWrite_tb <= '1';
    wait for 10 ns;

    wait;
  end process;
end sim;
