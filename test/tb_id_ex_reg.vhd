-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- tb_id_ex_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbench for the ID/EX Register.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_id_ex_reg is
end tb_id_ex_reg;

architecture testbench of tb_id_ex_reg is
    -- Constants and signals declaration
    constant N : integer := 155;
    
    signal iCLK        : std_logic := '0';
    signal iRST        : std_logic := '0';
    signal iWE         : std_logic := '0';
    signal control     : std_logic_vector(11 downto 0) := (others => '0');
    signal pcPlus4     : std_logic_vector(31 downto 0) := (others => '0');
    signal readData1   : std_logic_vector(31 downto 0) := (others => '0');
    signal readData2   : std_logic_vector(31 downto 0) := (others => '0');
    signal signExtend  : std_logic_vector(31 downto 0) := (others => '0');
    signal rsAddress   : std_logic_vector(4 downto 0) := (others => '0');
    signal rtAddress   : std_logic_vector(4 downto 0) := (others => '0');
    signal rdAddress   : std_logic_vector(4 downto 0) := (others => '0');
    signal oControl    : std_logic_vector(11 downto 0);
    signal oPcPlus4    : std_logic_vector(31 downto 0);
    signal oReadData1  : std_logic_vector(31 downto 0);
    signal oReadData2  : std_logic_vector(31 downto 0);
    signal oSignExtend : std_logic_vector(31 downto 0);
    signal oRsAddress  : std_logic_vector(4 downto 0);
    signal oRtAddress  : std_logic_vector(4 downto 0);
    signal oRdAddress  : std_logic_vector(4 downto 0);

    -- Component instantiation
    component id_ex_reg
        generic(N : integer := 155);
        port(
            iCLK        : in std_logic;
            iRST        : in std_logic;
            iWE         : in std_logic;
            control     : in std_logic_vector(11 downto 0);
            pcPlus4     : in std_logic_vector(31 downto 0);
            readData1   : in std_logic_vector(31 downto 0);
            readData2   : in std_logic_vector(31 downto 0);
            signExtend  : in std_logic_vector(31 downto 0);
            rsAddress   : in std_logic_vector(4 downto 0);
            rtAddress   : in std_logic_vector(4 downto 0);
            rdAddress   : in std_logic_vector(4 downto 0);
            oControl    : out std_logic_vector(11 downto 0);
            oPcPlus4    : out std_logic_vector(31 downto 0);
            oReadData1  : out std_logic_vector(31 downto 0);
            oReadData2  : out std_logic_vector(31 downto 0);
            oSignExtend : out std_logic_vector(31 downto 0);
            oRsAddress  : out std_logic_vector(4 downto 0);
            oRtAddress  : out std_logic_vector(4 downto 0);
            oRdAddress  : out std_logic_vector(4 downto 0));
    end component;

begin
    -- DUT (Design Under Test) instantiation
    DUT: id_ex_reg
        generic map(N => N)
        port map(
            iCLK        => iCLK,
            iRST        => iRST,
            iWE         => iWE,
            control     => control,
            pcPlus4     => pcPlus4,
            readData1   => readData1,
            readData2   => readData2,
            signExtend  => signExtend,
            rsAddress   => rsAddress,
            rtAddress   => rtAddress,
            rdAddress   => rdAddress,
            oControl    => oControl,
            oPcPlus4    => oPcPlus4,
            oReadData1  => oReadData1,
            oReadData2  => oReadData2,
            oSignExtend => oSignExtend,
            oRsAddress  => oRsAddress,
            oRtAddress  => oRtAddress,
            oRdAddress  => oRdAddress
        );

    -- Clock process
    process
    begin
        while now < 100 ns loop
            iCLK <= not iCLK; -- Toggle clock every half period
            wait for 5 ns;     -- Wait for 5 ns for the next clock edge
        end loop;
        wait;
    end process;

    -- Stimulus process (you can modify this part to apply different test cases)
    process
    begin
        wait for 10 ns; -- Initial wait
        iWE <= '1';     -- Enable write
        wait for 10 ns;
        -- Apply input values for testing
        control <= "000000000001";
        pcPlus4 <= "00000000000000000000000000001100";
        readData1 <= "00000000000000000000000000001010";
        readData2 <= "00000000000000000000000000010010";
        signExtend <= "00000000000000000000000000001100";
        rsAddress <= "00010";
        rtAddress <= "00101";
        rdAddress <= "01010";
        wait for 10 ns;
        iWE <= '0'; -- Disable write
        wait;
    end process;

end testbench;
