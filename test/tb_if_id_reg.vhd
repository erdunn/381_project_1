-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- tb_if_id_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbech for the IF/ID Register.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_if_id_reg is
end tb_if_id_reg;

architecture testbench of tb_if_id_reg is
    -- Constants and signals declaration
    constant N : integer := 98;
    
    signal iCLK        : std_logic := '0';
    signal iRST        : std_logic := '0';
    signal iWE         : std_logic := '0';
    signal stall       : std_logic := '0';
    signal flush       : std_logic := '0';
    signal currentPC   : std_logic_vector(31 downto 0) := (others => '0');
    signal pcPlus4     : std_logic_vector(31 downto 0) := (others => '0');
    signal instruction : std_logic_vector(31 downto 0) := (others => '0');
    signal oCurrentPC  : std_logic_vector(31 downto 0);
    signal oPCPlus4    : std_logic_vector(31 downto 0);
    signal oInstruction: std_logic_vector(31 downto 0);
    signal oStall      : std_logic;
    signal oFlush      : std_logic;

    -- Component instantiation
    component if_id_reg
        generic(N : integer := 98);
        port(
            iCLK        : in std_logic;
            iRST        : in std_logic;
            iWE         : in std_logic;
            stall       : in std_logic;
            flush       : in std_logic;
            currentPC   : in std_logic_vector(31 downto 0);
            pcPlus4     : in std_logic_vector(31 downto 0);
            instruction : in std_logic_vector(31 downto 0);
            oCurrentPC  : out std_logic_vector(31 downto 0);
            oPCPlus4    : out std_logic_vector(31 downto 0);
            oInstruction: out std_logic_vector(31 downto 0);
            oStall      : out std_logic;
            oFlush      : out std_logic
        );
    end component;

begin
    -- DUT (Design Under Test) instantiation
    DUT: if_id_reg
        generic map(N => N)
        port map(
            iCLK        => iCLK,
            iRST        => iRST,
            iWE         => iWE,
            stall       => stall,
            flush       => flush,
            currentPC   => currentPC,
            pcPlus4     => pcPlus4,
            instruction => instruction,
            oCurrentPC  => oCurrentPC,
            oPCPlus4    => oPCPlus4,
            oInstruction=> oInstruction,
            oStall      => oStall,
            oFlush      => oFlush
        );

    -- Clock process
    process
    begin
        while now < 100 ns loop
            iCLK <= not iCLK; -- Toggle clock every half period
            wait for 5 ns;     -- Wait for 5 ns for the next clock edge
        end loop;
        wait;
    end process;

    -- Stimulus process (you can modify this part to apply different test cases)
    process
    begin
        wait for 10 ns; -- Initial wait
        iWE <= '1';     -- Enable write
        wait for 10 ns;
        -- Apply input values for testing
        stall <= '1';
        flush <= '0';
        currentPC <= "00000000000000000000000000000000";
        pcPlus4   <= "00000000000000000000000000001000";
        instruction <= "00000000000000000000000000001111";
        wait for 10 ns;
        iWE <= '0'; -- Disable write
        wait;
    end process;

end testbench;
