library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_dmem is
end tb_dmem;

architecture sim of tb_dmem is
    constant DATA_WIDTH : natural := 32;
    constant ADDR_WIDTH : natural := 10;
    
    signal clk_tb : std_logic := '0';
    signal addr_tb : std_logic_vector(ADDR_WIDTH - 1 downto 0);
    signal data_tb : std_logic_vector(DATA_WIDTH - 1 downto 0);
    signal we_tb : std_logic := '1';
    signal q_tb : std_logic_vector(DATA_WIDTH - 1 downto 0);
    
    -- Memory initialization
    type memory_t is array(2**ADDR_WIDTH-1 downto 0) of std_logic_vector(DATA_WIDTH - 1 downto 0);
    signal memory : memory_t;
    
begin
    -- Initialize memory with initial values
    process
    begin
        memory(0)  <= x"00000001"; -- Initial value at address 0x00
        memory(1)  <= x"00000002"; -- Initial value at address 0x01
        memory(2)  <= x"00000003"; -- Initial value at address 0x02
        memory(3)  <= x"00000004"; -- Initial value at address 0x03
        memory(4)  <= x"00000005"; -- Initial value at address 0x04
        memory(5)  <= x"00000006"; -- Initial value at address 0x05
        memory(6)  <= x"00000007"; -- Initial value at address 0x06
        memory(7)  <= x"00000008"; -- Initial value at address 0x07
        memory(8)  <= x"00000009"; -- Initial value at address 0x08
        memory(9)  <= x"0000000A"; -- Initial value at address 0x09
        wait;
    end process;

    -- Clock generation
    clk_process: process
    begin
        while now < 1000 ns loop
            clk_tb <= not clk_tb;
            wait for 5 ns;
        end loop;
        wait;
    end process;
    
    -- Memory write and read tests
    mem_process: process
    begin
        -- Write back the values to consecutive locations starting at 0x100
        for i in 0 to 9 loop
            addr_tb <= std_logic_vector(to_unsigned(i + 256, ADDR_WIDTH)); -- 0x100 to 0x109
            data_tb <= memory(i);
            we_tb <= '1';
            wait for 10 ns;
        end loop;
        
        -- Read the new values and verify
        for i in 0 to 9 loop
            addr_tb <= std_logic_vector(to_unsigned(i + 256, ADDR_WIDTH)); -- 0x100 to 0x109
            we_tb <= '0';
            wait for 10 ns;
            assert q_tb = memory(i) report "Data mismatch at address " & integer'image(i + 256) severity error;
        end loop;
        
        wait;
    end process;
    
    -- Instantiate the mem module (data memory)
    dmem: entity work.mem
        generic map (
            DATA_WIDTH => DATA_WIDTH,
            ADDR_WIDTH => ADDR_WIDTH
        )
        port map (
            clk => clk_tb,
            addr => addr_tb,
            data => data_tb,
            we => we_tb,
            q => q_tb
        );
    
end sim;

