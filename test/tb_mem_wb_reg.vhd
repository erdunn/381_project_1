-------------------------------------------------------------------------
-- Alex Bashara
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------
-- MEM_WB_reg.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a testbench for the MEM/WB Register.
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity tb_mem_wb_reg is
end tb_mem_wb_reg;

architecture testbench of tb_mem_wb_reg is
    -- Constants and signals declaration
    constant N : integer := 71;
    
    signal iCLK        : std_logic := '0';
    signal iRST        : std_logic := '0';
    signal iWE         : std_logic := '0';
    signal control     : std_logic_vector(1 downto 0) := (others => '0');
    signal regDst      : std_logic_vector(4 downto 0) := (others => '0');
    signal memRead     : std_logic_vector(31 downto 0) := (others => '0');
    signal ALURes      : std_logic_vector(31 downto 0) := (others => '0');
    signal oControl    : std_logic_vector(1 downto 0);
    signal oRegDst     : std_logic_vector(4 downto 0);
    signal oMemRead    : std_logic_vector(31 downto 0);
    signal oALURes     : std_logic_vector(31 downto 0);

    -- Component instantiation
    component mem_wb_reg
        generic(N : integer := 71);
        port(
            iCLK       : in std_logic;
            iRST       : in std_logic;
            iWE        : in std_logic;
            control    : in std_logic_vector(1 downto 0);
            regDst     : in std_logic_vector(4 downto 0);
            memRead    : in std_logic_vector(31 downto 0);
            ALURes     : in std_logic_vector(31 downto 0);
            oControl   : out std_logic_vector(1 downto 0);
            oRegDst    : out std_logic_vector(4 downto 0);
            oMemRead   : out std_logic_vector(31 downto 0);
            oALURes    : out std_logic_vector(31 downto 0)
        );
    end component;

begin
    -- DUT (Design Under Test) instantiation
    DUT: mem_wb_reg
        generic map(N => N)
        port map(
            iCLK       => iCLK,
            iRST       => iRST,
            iWE        => iWE,
            control    => control,
            regDst     => regDst,
            memRead    => memRead,
            ALURes     => ALURes,
            oControl   => oControl,
            oRegDst    => oRegDst,
            oMemRead   => oMemRead,
            oALURes    => oALURes
        );

    -- Clock process
    process
    begin
        while now < 100 ns loop
            iCLK <= not iCLK; -- Toggle clock every half period
            wait for 5 ns;     -- Wait for 5 ns for the next clock edge
        end loop;
        wait;
    end process;

    -- Stimulus process (you can modify this part to apply different test cases)
    process
    begin
        wait for 10 ns; -- Initial wait
        iWE <= '1';     -- Enable write
        wait for 10 ns;
        -- Apply input values for testing
        control <= "00";
        regDst <= "00101";
        memRead <= (others => '1');
        ALURes <= "00000000000000000000000000001111";
        wait for 10 ns;
        iWE <= '0'; -- Disable write
        wait;
    end process;

end testbench;
