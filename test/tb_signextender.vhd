library ieee;
use ieee.std_logic_1164.all;


entity tb_signextender is
end tb_signextender;

architecture sim of tb_signextender is
    signal input : std_logic_vector(15 downto 0) := "1010101010101010";
    signal output : std_logic_vector(31 downto 0);
    component sign_extender is
        generic (
            INPUT_WIDTH : natural := 16
        );
        port (
            input : in std_logic_vector(INPUT_WIDTH - 1 downto 0);
            output : out std_logic_vector(31 downto 0)
        );
    end component;
begin
    
    
    extender: sign_extender generic map (
        INPUT_WIDTH => 16
    ) port map (
        input => input,
        output => output
    );

    
end sim;
