library ieee;
use ieee.std_logic_1164.all;

entity tb_zeroextender is
end tb_zeroextender;

architecture sim of tb_zeroextender is
    signal input : std_logic_vector(15 downto 0) := "1010101010101010";
    signal output : std_logic_vector(31 downto 0);
    component zero_extender is
        generic (
            INPUT_WIDTH : natural := 16
        );
        port (
            input : in std_logic_vector(INPUT_WIDTH - 1 downto 0);
            output : out std_logic_vector(31 downto 0)
        );
    end component;
begin
    
    
    extender: zero_extender generic map (
        INPUT_WIDTH => 16
    ) port map (
        input => input,
        output => output
    );

   
end sim;
